@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Fire Extinguishers</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Fire Extinguishers</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647759180/x8b5g6am03xkpocggpso.png" alt="Fire Extinguishers"></figure>
                        <div class="text">
                            <h2>Fire Extinguishers</h2>
                            <p></p><div autocomplete="off"><b>“ Fire is a good servant but a bad master. “</b></div><div>Specialized in supplying fire extinguishers that are appropriate and rated to fully cover the fire risks within your environment.</div><div><br></div><div>Our fully trained engineers also ensure that your fire extinguishers are installed according to current standards.</div><div><br></div><div>Ensuring you have fire extinguishers that are properly serviced and should be maintained is a vital part of fire protection requirements.</div><div><br></div><div>Advance Fire Protection system understands how important, for both life safety and premises protection and as such provides comprehensive fire extinguisher servicing and maintenance packages.</div><p></p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection