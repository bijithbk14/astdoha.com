@extends('frontend.base')
@section('content')
    <section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
        <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
        <div class="auto-container">
            <div class="content-box">
                <div class="title">
                    <h1>Gallery</h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Gallery</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="gallery-page-section">
        <div class="auto-container">
            <div class="row clearfix">
                @if(count($gallery)!=0)
                @foreach($gallery as $item)
                <div class="col-md-12 text mt-4 mb-4">
                    <h2 class="text-center">{{$item['category']}}</h2>
                </div>
                @foreach ($item['items'] as $data)
                <div class="col-lg-4 col-md-6 col-sm-12 gallery-block">
                    <div class="gallery-block-one">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="{{$data['image']}}" alt="{{$data['title']}}">
                                <a href="{{$data['image']}}" class="view-btn lightbox-image"
                                    data-fancybox="gallery">+</a>
                            </figure>
                        </div>
                    </div>
                </div>
                @endforeach
                @endforeach
                @else
                <div class="col-md-12">
                    <div class="sec-title text-center">
                        <h2>No Data Found</h2>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection
