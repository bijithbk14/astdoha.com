@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>HVAC Projects Division</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>HVAC Projects Division</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.service-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647607049/nigu6kwcws8msw1on8wn.png" alt="HVAC Projects Division"></figure>
                        <div class="text">
                            <h2>HVAC Projects Division</h2>
                            <p></p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px;" autocomplete="off"><b><font size="3">⥤ &nbsp;&nbsp;<span style="font-family: ralewayb;">Chilled Water Piping</span></font></b></p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px; text-align: justify;">Chilled water systems work much the same way as direct expansion systems work. The exception is they use water in the coil rather than refrigerant. Technically speaking, water can be classified as a refrigerant. Chilled Water systems can be rather complex and many chilled water systems are found in commercial and industrial applications. There are some chilled water systems used in residential applications.</p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px; text-align: justify;">However, chilled water systems in residential HVAC systems are extremely rare. A typical chiller uses the process of refrigeration to chill water in a chiller barrel. This water is pumped through chilled water piping throughout the building where it will pass through a coil. Air is passed over this coil and the heat exchange process takes place. The heat in the air is absorbed into the coils and then into the water. The water is pumped back to the chiller to have the heat removed. It then makes the trip back through the building and the coils all over again.</p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px;"><b><font size="3">⥤ &nbsp;&nbsp;<span style="font-family: ralewayb;">Ducting</span></font></b></p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px; text-align: justify;">HVAC (an initialism meaning “heating, ventilation and air conditioning”) systems are something that greatly benefits our modern society, but is often taken for granted. HVAC systems are necessary to keep indoor spaces warm, cool, and full of high-quality and clean air. In order to have an effective system, you need to have quality HVAC air ducts. At Ducting.com, we specialize in flexible hoses for heating, cooling, and venting in outside air to maintain high-quality air control. Whether you want to cool a car, house, or industrial-sized building, we have the right air duct hoses for the task.</p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px;"><b><font size="4">⥤ &nbsp;&nbsp;<span style="font-family: ralewayb;">Installation &amp; Maintenance</span></font></b></p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px; text-align: justify;">A new HVAC system represents a big investment, and it’s understandable that some home or business owners will want to cut costs on installation. This can be a grave error if they pick an unqualified contractor to install air conditioning units, as a shoddy installation job can result in long-term problems and expenses.</p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px; text-align: justify;">Heating and air conditioning units and their associated ductwork and infrastructure are complex systems that require precise installation. If the job’s not done right, the system won’t perform efficiently and may be more prone to breakage and have a shorter life. Energy Star, a federal program that evaluates appliances for efficiency and environmental impact, estimates that about half of all heating and cooling systems don’t perform as they should because they were improperly installed.</p><p style="margin-bottom: 10px; color: rgb(51, 51, 51); font-family: Segoe, &quot;Segoe UI&quot;, &quot;DejaVu Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, sans-serif; font-size: 14.1333px;">An HVAC system that’s not providing optimum performance can be costly for home and business owners. HVAC systems account for about 40 percent or more of our energy consumption. Energy Star estimates that improper installation can reduce system efficiency by up to 30 percent.</p><p></p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            
                                            
                                            
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection