@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Passive Fire Protections</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Passive Fire Protections</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647758935/g6l2f02mm8egwowwfztf.png" alt="Passive Fire Protections"></figure>
                        <div class="text">
                            <h2>Passive Fire Protections</h2>
                            <p>By containing and compartmentalizing a fire and preventing it from spreading throughout the building, fire doors are essential for life safety and property protection. We, Advance Fire Protection we specialize in passive fire protection.</p>
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column" autocomplete="off">
                                    <div class="text">
                                        <ul class="list clearfix">
                                            <li>Fire Rated Steel and Wooden Doors</li>
                                            <li>Penetration Sealants</li>
                                            <li>Intumescent Coatings</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


            
        
    @section('scripts')

@endsection
@endsection