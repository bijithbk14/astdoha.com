
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="crf-token" content="{{ csrf_token() }}">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{$page->meta_title ? $page->meta_title.' - '.$meta_data['title'] : $meta_data['title']}}</title>
<meta name="description" content="{{$page->meta_description ? $page->meta_description : $meta_data['description']}}">
<!-- Fav Icon -->
<link rel="icon" href="{{ asset('frontend/assets/images/favicon.ico')}}" type="image/x-icon">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700&amp;display=swap" rel="stylesheet">
<!-- Stylesheets -->
<link href="{{ asset('frontend/assets/css/font-awesome-all.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/flaticon.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/owl.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/jquery.fancybox.min.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/animate.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/color.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/cusotm.css')}}" rel="stylesheet">
<link href="{{ asset('frontend/assets/css/responsive.css')}}" rel="stylesheet">

</head>
<!-- page wrapper -->
<body>
    <div class="boxed_wrapper">
        <div class="loader-wrap" remove-element-vvveb="">
            <div class="preloader">
                <div class="preloader-close">Preloader Close</div>
                <div id="handle-preloader" class="handle-preloader">
                    <div class="animation-preloader">
                        <div class="spinner"></div>
                        <div class="txt-loading">
                            <span data-text-preloader="A" class="letters-loading">
                                A
                            </span>
                            <span data-text-preloader="T" class="letters-loading">
                                T
                            </span>
                            <span data-text-preloader="S" class="letters-loading">
                                S
                            </span>
                            <span data-text-preloader="D" class="letters-loading">
                                D
                            </span>
                            <span data-text-preloader="O" class="letters-loading">
                                O
                            </span>
                            <span data-text-preloader="H" class="letters-loading">
                                H
                            </span>
                            <span data-text-preloader="A" class="letters-loading">
                                A
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <header class="main-header" remove-element-vvveb="">
            <div class="header-top">
                <!-- header-top -->
                <div class="auto-container">
                    <div class="top-inner clearfix">
                        <div class=" pull-left"><p><a href="tel:+974 44671539" class="text-dark">+974 44671539</a> | <a href="mailto:sales@atsdoha.com" class="text-dark">sales@atsdoha.com</a></p></div>
                        <div class="top-right pull-right">
                            <ul class="social-links clearfix">
                                <li><a target="_blank" href="https://twitter.com/ATSDOHA"><i class="fab fa-twitter"></i></a></li>
                                <li><a target="_blank" href="https://www.facebook.com/atsdoha"><i class="fab fa-facebook-square"></i></a></li>
                                <li><a target="_blank" href="https://wa.me/97470607079/?text=Hello"><i class="fab fa-whatsapp"></i></a></li>
                                <li><a target="_blank" href="https://www.instagram.com/atsdoha/"><i class="fab fa-instagram"></i></a></li>
                                <li><a target="_blank" href="https://www.linkedin.com/in/advance-technical-services-advance-fire-protection-system-402750a7/"><i class="fab fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- header-lower -->
            <div class="header-lower">
                <div class="auto-container">
                    <div class="outer-box clearfix">
                        <div class="logo-box pull-left">
                            <figure class="logo"><a href="/"><img src="{{ asset('frontend/assets/images/logo.png')}}" alt=""></a></figure>
                        </div>
                        <div class="menu-area pull-right">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler">
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                            </div>
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        @foreach ($menus as $menu)
                                        <li class="current dropdown">
                                            @if ($menu['slug']=="more")
                                            <a href="#">
                                                {{$menu["menu"]}}
                                            </a>
                                            @else
                                            <a href="{{route('pages',$slug=$menu['slug'])}}">
                                                {{$menu["menu"]}}
                                            </a>
                                            @endif

                                            @if (count($menu["items"])>0)
                                                <ul>
                                                    @foreach ($menu["items"] as $submenu)
                                                        <li><a href="{{route('pages',$slug=$submenu['sub_menu_slug'])}}">{{$submenu['sub_menu']}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <!-- <div class="support-box pull-right">
                            <i class="flaticon-call"></i>
                            <p>Emergency Call</p>
                            <h4><a href="tel:6668880000">666 888 0000</a></h4>
                        </div> -->
                    </div>
                </div>
            </div>

            <!--sticky Header-->
            <div class="sticky-header">
                <div class="auto-container">
                    <div class="outer-box clearfix">
                        <div class="logo-box pull-left">
                            <figure class="logo"><a href="/"><img src="{{ asset('frontend/assets/images/logo.png')}}" alt=""></a></figure>
                        </div>
                        <div class="menu-area pull-right">
                            <nav class="main-menu clearfix">
                                <!--Keep This Empty / Menu will come through Javascript-->
                            </nav>
                        </div>
                        <!-- <div class="support-box pull-right">
                            <i class="flaticon-call"></i>
                            <p>Emergency Call</p>
                            <h4><a href="tel:6668880000">666 888 0000</a></h4>
                        </div> -->
                    </div>
                </div>
            </div>
        </header>
        <div class="mobile-menu" remove-element-vvveb="">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>

            <nav class="menu-box">
                <div class="nav-logo"><a href="/"><img src="{{ asset('frontend/assets/images/logo-2.png')}}" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            </nav>
        </div>
        @yield('content')
        <footer class="main-footer" style="background-image: url(/frontend/assets/images/background/footer-bg-1.jpg);" remove-element-vvveb="">
            <figure class="footer-logo"><img src="assets/images/footer-logo.png" alt=""></figure>
            <div class="footer-top">
                <div class="auto-container">
                    <div class="widget-section">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                                <div class="footer-widget about-widget">
                                    <div class="widget-title">
                                        <h4>ATSDOHA</h4>
                                    </div>
                                    <div class="text">
                                        <p>
                                           We are the most professional and reliable fire protection & safety solutions provider in Qatar. We have abundant experience and solution to help you through our customer care.
                                        </p>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6  col-md-offset-1 col-sm-12 footer-column">
                                <div class="footer-widget links-widget"  style="margin-left: 65px;">
                                    <div class="widget-title">
                                        <h4>Navigation</h4>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="links-list clearfix">
                                            @foreach ($menus as $menu)
                                            @if($menu['slug']=='more')
                                                @php continue @endphp
                                            @endif
                                            <li><a href="{{route('pages',$slug=$menu['slug'])}}">{{$menu["menu"]}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                                <div class="footer-widget links-widget" style="margin-left: 30px;">
                                    <div class="widget-title">
                                        <h4>More</h4>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="links-list clearfix">
                                            @foreach ($menus as $menu)
                                            @if($menu['slug']=='more')
                                                @foreach ($menu['items'] as $item)
                                                <li><a href="{{route('pages',$slug=$item['sub_menu_slug'])}}">{{$item["sub_menu"]}}</a></li>
                                                @endforeach
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                                <div class="footer-widget map-widget">
                                    <div class="widget-title">
                                        <h4>Find On Map</h4>
                                    </div>
                                    <div class="map-inner">
                                        <div class="map-container">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3609.121345146004!2d51.48347941501022!3d25.232837583879675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45c54fdc74e66d%3A0x71833df98c493ea1!2sADVANCE%20TECHNICAL%20SERVICES%20W.L.L-Advance%20Fire%20Protection%20System!5e0!3m2!1sen!2sin!4v1647269856059!5m2!1sen!2sin" width="100%" height="173px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="auto-container clearfix">
                    <div class="copyright pull-left">
                        <p>Copyright &copy; 2022 Advance Technical Services W.L.L . All Rights Reserved </p>
                    </div>
                    <ul class="footer-social clearfix pull-right">
                        <li><a target="_blank" href="https://twitter.com/ATSDOHA"><i class="fab fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/atsdoha"><i class="fab fa-facebook-square"></i></a></li>
                        <li><a target="_blank" href="https://wa.me/97470607079/?text=Hello"><i class="fab fa-whatsapp"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/atsdoha/"><i class="fab fa-instagram"></i></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/in/advance-technical-services-advance-fire-protection-system-402750a7/"><i class="fab fa-linkedin"></i></a></li>
                        <li><a target="_blank" href="https://www.google.com.qa/maps/place/ADVANCE+TECHNICAL+SERVICES+W.L.L-Advance+Fire+Protection+System/@25.2715305,51.5216057,17z/data=!3m1!4b1!4m5!3m4!1s0x3e45c54fdc74e66d:0x71833df98c493ea1!8m2!3d25.2715305!4d51.5237944?hl=en&authuser=3/"><i class="fas fa-directions"></i></a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <button class="scroll-top scroll-to-target" data-target="html" remove-element-vvveb="">
            <span class="fa fa-arrow-up"></span>
        </button>
    </div>

    <script src="{{ asset('frontend/assets/js/jquery.js')}}" remove-element-vvveb=""></script>
    <script src="{{ asset('frontend/assets/js/popper.min.js')}}" remove-element-vvveb=""></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.min.js')}}" remove-element-vvveb=""></script>
    @yield('scripts')
    <script src="{{ asset('frontend/assets/js/validation.js')}}" remove-element-vvveb=""></script>
    <script src="{{ asset('frontend/assets/js/jquery.fancybox.js')}}" remove-element-vvveb=""></script>
    <script src="{{ asset('frontend/assets/js/appear.js')}}" remove-element-vvveb=""></script>
    <script src="{{ asset('frontend/assets/js/jquery.countTo.js')}}" remove-element-vvveb=""></script>
    <script src="{{ asset('frontend/assets/js/script.js')}}" remove-element-vvveb=""></script>

    <script remove-element-vvveb="">
        var url = 'https://wati-integration-service.clare.ai/ShopifyWidget/shopifyWidget.js?86198';
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url;
        var options = {
            "enabled": true,
            "chatButtonSetting": {
                "backgroundColor": "#4dc247",
                "ctaText": "",
                "borderRadius": "25",
                "marginLeft": "20",
                "marginBottom": "50",
                "marginRight": "100",
                "position": "left"
            },
            "brandSetting": {
                "brandName": "Advance Technical Services W.L.L",
                "brandSubTitle": "Typically replies within an hour",
                "brandImg": "/frontend/assets/images/footer-logo.png",
                "welcomeText": "Hi there!\nHow can I help you?",
                "messageText": "Hello, I have a question",
                "backgroundColor": "#0a5f54",
                "ctaText": "Start Chat",
                "borderRadius": "25",
                "autoShow": false,
                "phoneNumber": "+974 7060 7079"
            }
        };
        s.onload = function() {
            CreateWhatsappChatWidget(options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    </script>
</body>
</html>
