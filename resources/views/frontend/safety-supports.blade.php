@extends('frontend.base')
@section('content')
<div class="boxed_wrapper">
        <section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
            <div class="overlay-bg"></div>
            <div class="pattern-layer"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                        <h1>Safety Supports</h1>
                    </div>
                    <ul class="bread-crumb clearfix">
                        <li><a href="/">Home</a></li>
                        <li>Safety Supports</li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="service-details" autocomplete="off">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
                        @include('frontend.includes.service-side-bar')
                    </div>
                    <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                        <div class="service-details-content">
                            <div class="inner-box">
                                <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647521519/uuoekfxl4cdzrymgxkec.png" alt="Safety Supports"></figure>
                                <div class="text">
                                    <h2>Safety Supports</h2>
                                    <p></p><div>Occupational Health &amp; Safety Services and Project Support:</div><div>We are providing Occupational Health Safety and Life safety supports to the projects with NEBOSH and other certified Engineers / Officers.</div><p></p>

                                </div>
                                <div class="two-column">
                                    <div class="row align-items-center clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                            <div class="text">


                                                <ul class="list clearfix">






                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>




    @section('scripts')

@endsection
@endsection
