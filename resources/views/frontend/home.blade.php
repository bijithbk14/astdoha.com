@extends('frontend.base')
@section('content')
    <div class="boxed_wrapper">
            <section class="banner-section style-one">
        <div class="pattern-layer"></div>
        <div class="banner-carousel owl-theme owl-carousel owl-nav-none">
            <div class="slide-item bg-left">
                <div class="image-layer" style="background-image:url(/frontend/assets/images/banner/banner1.png)"></div>
                <div class="auto-container">
                    <div class="content-box">
                        <h1>FIRE DETECTION / ALARM SYSTEM</h1>
                        <p>With intense fire safety, power, firefighting, and life safety services, ATSDOHA is the first
                            choice in the UAE market..</p>
                        <div class="btn-box">
                            <a href="/Advance-Fire-ATS-BROCHURE.pdf" target="_blank" class="theme-btn-one">Download Brochure</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item bg-right">
                <div class="image-layer" style="background-image:url(/frontend/assets/images/banner/banner3.png)"></div>
                <div class="auto-container">
                    <div class="content-box">
                        <h1>FIRE <br>FIGHTING SYSTEM</h1>
                        <p>With intense fire safety, power, firefighting, and life safety services, ATSDOHA is the first
                            choice in the UAE market..</p>
                        <div class="btn-box">
                            <a href="/Advance-Fire-ATS-BROCHURE.pdf" target="_blank" class="theme-btn-one">Download Brochure</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item bg-left">
                <div class="image-layer" style="background-image:url(/frontend/assets/images/banner/banner4.png)"></div>
                <div class="auto-container">
                    <div class="content-box">
                        <h1>FIRE SUPPRESSION SYSTEM</h1>
                        <p>With intense fire safety, power, firefighting, and life safety services, ATSDOHA is the first
                            choice in the UAE market..</p>
                        <div class="btn-box">
                            <a href="/Advance-Fire-ATS-BROCHURE.pdf" target="_blank" class="theme-btn-one">Download Brochure</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item bg-right">
                <div class="image-layer" style="background-image:url(/frontend/assets/images/banner/banner2.png)"></div>
                <div class="auto-container">
                    <div class="content-box">
                        <br><br>
                        <h1>AMC <br>INSPECTION</h1>
                        <p>With intense fire safety, power, firefighting, and life safety services, ATSDOHA is the first
                            choice in the UAE market..</p>
                        <div class="btn-box">
                            <a href="/Advance-Fire-ATS-BROCHURE.pdf" class="theme-btn-one">Download Brochure</a>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <div class="image_block_1">
                        <div class="image-box">
                            <figure class="image"><img src="/frontend/assets/images/about/about.png" alt="">
                            </figure>
                            <div class="image-content">
                                <h2>10</h2>
                                <h5>Years of expeirece in this field</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div class="content_block_1">
                        <div class="content-box">
                            <div class="sec-title">
                                <p>About ATS Doha</p>
                                <h2>Advance Technical Services W.L.L</h2>
                            </div>
                            <div class="text">
                                <p>
                                    Is an IMS Certified (ISO 9001:2015, ISO 14001:2015[EMS], 18001:2007[OHSAS]) and Qatar
                                    Civil Defense Department (QCDD) approved electromechanical Contracting &amp; Trading Company
                                    specializing in Fire Protection, Fire Detection, MEP contracts, HSE Services and
                                    Trading. We have been started our operations in the state of Qatar since 2008. Now we
                                    are one among the leading Fire System Company in Qatar Fire Protection &amp; Fire detection
                                    industry &amp; MEP Sector. “Advance Fire Protection System” offers a wide range of
                                    engineering solutions, Design, Supplies of materials, New Installations, Repair &amp;
                                    Maintenance, ...
                                    <a href="/pages/about-us" class="text-dark"><b>Read More</b></a>
                                </p>
                            </div>
                            <div class="inner-box clearfix">
                                <div class="single-item">
                                    <i class="flaticon-bonfire"></i>
                                    <h4>Think of fire <br>before it starts</h4>
                                </div>
                                <div class="single-item">
                                    <i class="flaticon-matches"></i>
                                    <h4>Don’t play with matches</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="service-section centred bg-color-1">
        <div class="bg-layer" style="background-image: url(/frontend/assets/images/background/service-bg-1.jpg);">
        </div>
        <div class="auto-container">
            <div class="sec-title">
                <p>A Safer Community Services</p>
                <h2>Services We’re Offering</h2>
            </div>
            <div class="row clearfix">
                @foreach($services as $item)
                {{$item->iteration}}
                @if ($loop->iteration == 5)
                        @continue
                @endif
                <div class="col-lg-4 col-md-6 col-sm-12 service-block mb-4">
                    <div class="service-block-one wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><img src="{{$item['thumbnail']}}" alt="{{$item['page_name']}}">
                            </figure>
                            <div class="text">
                                <h2>{{$item['page_name']}}</h2>
                            </div>
                            <div class="overlay-content">
                                <div class="inner">
                                    <h3><a href="{{route('pages',$slug=$item['slug'])}}">{{$item['page_name']}}</a></h3>
                                    <p>There are many of pass ages of <br>available but the suffered.</p>
                                    <div class="link"><a href="{{route('pages',$slug=$item['slug'])}}">More</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-md-12">
                    <div><a href="/pages/services" style="padding:10px" class="theme-btn-one">More Services</a>
                </div>
            </div>
        </div>
    </section>
    <section class="skrills-section bg-color-2">
        <div class="bg-layer" style="background-image: url(/frontend/assets/images/background/skrills-bg-1.jpg);">
        </div>
        <figure class="image-layer"><img src="/frontend/assets/images/resource/skrills-1.jpg" alt=""></figure>
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-12 col-md-12 content-column">
                    <div class="content_block_2">
                        <div class="content-box">
                            <div class="sec-title light">
                                <h2>Advanced Fire Protection System</h2>
                            </div>
                            <div class="text">
                                <p>
                                    We offer a wide range of Fire Engineering
                                    solutions, such as Designing, Supply of
                                    Materials, Installations, Testing &amp;
                                    Commissioning, Repairs &amp; Maintenance,
                                    Annual Preventive &amp; Corrective Maintenance
                                    (AMC) of Firefighting, Fire Detection, Fire
                                    Suppression System, Deluge, Smoke
                                    Ventilation, Exit &amp; Emergency light
                                    System, Public Addressable System, Etc.
                                </p>

                            </div>
                            <div class="text">
                                <p>
                                    Our fully trained QCDD Certified
                                    Engineers will ensure that your Fire
                                    Systems are installed according to the
                                    current NFPA Codes and as per the QCDD
                                    guidelines.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cta-section bg-color-3">
        <div class="pattern-layer" style="background-image: url(/frontend/assets/images/shape/pattern-1.png);"></div>
        <div class="auto-container">
            <div class="inner-box clearfix">
                <div class="text pull-left">
                    <h2>24/7 Available. Call us Immediately<br>
                        <div class="row">
                            <div class="col-lg-4">7060 7079</div>
                            <div class="col-lg-4">3308 4597</div>
                            <div class="col-lg-4">7048 0570</div>
                        </div>
                    </h2>
                </div>
                <div class="btn-box pull-right">
                    <a href="/pages/contact-us" class="theme-btn-one">Contact with us</a>
                </div>
            </div>
        </div>
    </section>
    <section class="gallery-section pt-6">
        <div class="outer-container">
            <div class="gallery-carousel owl-carousel owl-theme owl-dots-none owl-nav-none">
                <div class="gallery-block-one">
                    <div class="inner-box">
                        <figure class="image-box">
                            <img src="/frontend/assets/images/gallery/01.png" alt="">
                            <a href="/frontend/assets/images/gallery/01.png" class="view-btn lightbox-image" data-fancybox="gallery">+</a>
                        </figure>
                    </div>
                </div>
                <div class="gallery-block-one">
                    <div class="inner-box">
                        <figure class="image-box">
                            <img src="/frontend/assets/images/gallery/02.png" alt="">
                            <a href="/frontend/assets/images/gallery/02.png" class="view-btn lightbox-image" data-fancybox="gallery">+</a>
                        </figure>
                    </div>
                </div>
                <div class="gallery-block-one">
                    <div class="inner-box">
                        <figure class="image-box">
                            <img src="/frontend/assets/images/gallery/03.png" alt="">
                            <a href="/frontend/assets/images/gallery/03.png" class="view-btn lightbox-image" data-fancybox="gallery">+</a>
                        </figure>
                    </div>
                </div>
                <div class="gallery-block-one">
                    <div class="inner-box">
                        <figure class="image-box">
                            <img src="/frontend/assets/images/gallery/04.png" alt="">
                            <a href="/frontend/assets/images/gallery/04.png" class="view-btn lightbox-image" data-fancybox="gallery">+</a>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial-section">
        <div class="pattern-layer" style="background-image: url(/frontend/assets/images/shape/pattern-2.png);"></div>
        <div class="large-container">
            <div class="sec-title centred">
                <p>Their Testimonials</p>
                <h2>What They Say</h2>
            </div>
            <div class="three-item-carousel owl-carousel owl-theme owl-dots-none owl-nav-none">
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <figure class="author-thumb"><img src="/frontend/assets/images/resource/testimonial-1.png" alt=""></figure>
                        <div class="text">
                            <p>I was very impresed by the company services lorem ipsum is simply free text available used by
                                copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                        </div>
                        <div class="author-info">
                            <h4>Mike Hardson</h4>
                            <span class="designation">Our Citizen</span>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <figure class="author-thumb"><img src="/frontend/assets/images/resource/testimonial-2.png" alt=""></figure>
                        <div class="text">
                            <p>I was very impresed by the company services lorem ipsum is simply free text available used by
                                copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                        </div>
                        <div class="author-info">
                            <h4>Jessica Brown</h4>
                            <span class="designation">Our Citizen</span>
                        </div>
                    </div>
                </div>
                <div class="testimonial-block-one">
                    <div class="inner-box">
                        <figure class="author-thumb"><img src="/frontend/assets/images/resource/testimonial-3.png" alt=""></figure>
                        <div class="text">
                            <p>I was very impresed by the company services lorem ipsum is simply free text available used by
                                copytyping refreshing. Neque porro est qui dolorem ipsum quia.</p>
                        </div>
                        <div class="author-info">
                            <h4>Kevin Martin</h4>
                            <span class="designation">Our Citizen</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="clients-section bg-color-1">
        <div class="sec-title centred">
            <!-- <p>Brands Which We Supply</p> -->
            <h2>Brands Which We Supply</h2>
        </div>
        <div class="auto-container">
            <div class="clients-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/001.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/002.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/003.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/005.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/006.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/007.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/008.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/009.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/010.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/011.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/013.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/014.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/015.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/016.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/017.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/018.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/019.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/020.png" alt=""></a>
                </figure>
                <figure class="clients-logo-box"><a href="#"><img src="/frontend/assets/images/clients/021.png" alt=""></a>
                </figure>
            </div>
        </div>
    </section>
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title centred">
                <h2>Quality Services</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 news-block">
                    <div class="">
                        <p>
                            There is no time to gather valuables or make a phone call. In just two minutes, a fire can
                            become life-threatening. In five minutes, a residence can be engulfed in flames.
                            Heat and smoke from the fire can be more dangerous than flames. Inhaling the super-hot air can
                            sear your lungs. Fire produces poisonous gases that make you disoriented and drowsy. Instead of
                            being awakened by a fire, you may fall into a deeper sleep. Asphyxiation is the leading cause of
                            fire deaths, exceeding burns by a three-to-one ratio.
                        </p>
                        <br>
                        <p><b>
                                Understanding the "Fire Triangle" is the most basic concept in Fire Prevention and Control.
                                In order for any fire to occur, three critical elements must be present:
                            </b></p><br>
                        <ol class="text-danger">
                            <li>1. A fuel or combustible material.</li>
                            <li>2. An ignition or heat source.</li>
                            <li>3. Oxygen in sufficient quantities to support combustion.</li>
                        </ol>
                        <p>
                            When all three of these elements come together, combustion is the result. However, if only one
                            of these elements is removed from contact with the other two, the threat of fire can be
                            minimized. Thus, if oxygen, heat or the fuel supply can be removed, there is minimal risk of
                            fire.
                            Oxygen, heat, and fuel are frequently referred to as the "fire triangle." Add in the fourth
                            element, the chemical reaction, and you actually have a fire "tetrahedron." The important thing
                            to remember is: take any of these four things away, and you will not have a fire or the fire
                            will be extinguished.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
@section('scripts')
<script src="{{ asset('frontend/assets/js/owl.js')}}" class="additional-scripts-vvveb" remove-element-vvveb=""></script>
<script src="{{ asset('frontend/assets/js/wow.js')}}" class="additional-scripts-vvveb" remove-element-vvveb=""></script>

@endsection
@endsection
