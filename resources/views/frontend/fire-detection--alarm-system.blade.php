@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Fire Detection / Alarm System</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Fire Detection / Alarm System</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647757621/qydrkyiazcwuf7jbyyyt.png" alt="Fire Detection / Alarm System"></figure>
                        <div class="text">
                            <h2>Fire Detection / Alarm System</h2>
                            <p>Advance Fire Protection system Designs, Supplies and Installs the correct fire alarm systems for your environment assuring you of a reliable and efficient method of fire detection. Our fully trained engineers ensure that your fire alarm system is installed according to current guidelines.</p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            <li>Conventional Fire Alarm System</li>
                                            <li>Addressable Fire Alarm System</li>
                                            <li>About a fire escape plan and practice</li>
                                            <li>Standalone / Battery Operated Detectors</li>
                                            <li autocomplete="off">Open area Beam Detection System</li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection