@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>More</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>More</li>
                </ul>
             </div>
        </div>
    </section>
<div>
<p>Drag and drop content here</p>
</div>
@endsection
