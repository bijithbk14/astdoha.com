@extends('frontend.base')
@section('content')
    <section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
        <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
        <div class="auto-container">
            <div class="content-box">
                <div class="title">
                    <h1>About</h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="index.html">Home</a></li>
                    <li>About</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="sec-title text-center">
                        <h2>Advance Technical Services W.L.L</h2>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                    <div class="image_block_1">
                        <div class="">
                            <img src="/frontend/assets/images/about/about.png" alt="">

                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 content-column">
                    <div class="content_block_1">
                        <div class="content-box">
                            <div class="text">
                                <p>
                                    ADVANCE TECHNICAL SERVICES W.L.L (ATS DOHA) is an IMS Certified (ISO 9001:2015, ISO
                                    14001:2015[EMS], 18001:2007[OHSAS]) and Qatar Civil Defense Department (QCDD) approved
                                    electromechanical Contracting & Trading Company specializing in Fire Protection, Fire
                                    Detection, MEP contracts, HSE Services and Trading. We have been started our operations
                                    at the state of Qatar since 2008. Now we are one among the leading Fire System Company
                                    in Qatar Fire Protection & Fire detection industry & MEP Sector.

                                </p>
                                <br>
                                <p>
                                    Advance Fire Protection System offers a wide range of engineering solutions, Design,
                                    Supplies of materials, New Installations, Repair & Maintenance, Upgrading, Testing &
                                    commissioning of Fire Fighting, Fire Detection, and Fire Suppression system. Also, we
                                    offer the services required for Qatar Civil Defense Department (QCDD) such as Annual
                                    Maintenance Contracts (AMC), New Installations, Inspection & Test Reports, Repair&
                                    Maintenance, etc. We deliver a full range of fire protection solutions to businesses and
                                    individuals, which are designed to be cost-effective at all times. With a speedy
                                    response service, the business also has polite and friendly, experienced personnel
                                    enabling clients to understand and comply with current regulations.
                                </p>
                                <br>
                                <q>
                                    <b>With a speedy response service, the business also has polite and friendly,
                                        experienced personnel enabling clients to understand and comply with current
                                        regulations.</b>
                                </q>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="sec-title text-center">
                        <h2>Our Team</h2>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-8">
                    <p>
                        We have Qatar Civil Defence Certified and experienced Engineers, Experienced Supervisors &
                        Technicians for the project Installation & Maintenance works. Our team is committed to identifying
                        the needs of our customers in an efficient manner. Our experienced team is familiar with the latest
                        codes and standards and is trained to identify and meet the needs of our customers.
                    </p>
                    <br>
                    <q>
                        <b> With a speedy response service, the business also has polite and friendly, experienced personnel
                            enabling clients to understand and comply with current regulations.</b>
                    </q>
                </div>
                <div class="col-md-4">
                    <img src="/frontend/assets/images/about/team.png" alt="">
                </div>
                <div class="col-md-12">
                    <div class="sec-title text-center mt-4 mb-3">
                        <h2>Our Vision</h2>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="/frontend/assets/images/about/vision.png" alt="">
                </div>
                <div class="col-md-8">
                    <p>
                        We recognize that to deliver the highest quality of service excellence must be a significant of the
                        construction and operation of the organization.
                    </p>
                    <hr>
                    <q>
                        <b>
                            The use of silly and meaningless safety language matters, it creates a distraction and delusion
                            that safety and risk are being addressed. We may feel good about speaking such words but they
                            dumb down culture and distract people from taking safety seriously
                            - Dr. Rob Long
                        </b>
                    </q>
                    <hr>
                </div>
                <div class="col-md-12">
                    <div class="sec-title text-center mt-4 mb-3">
                        <h2>Our Mission</h2>
                        <p></p>
                    </div>
                </div>

                <div class="col-md-8">
                    <p>
                        The company will make every effort to deliver the highest quality products and services in the
                        business. To perform for our customers the highest level of quality construction services at fair
                        and market competitive prices.
                    </p>
                    <p>
                        To ensure the permanency of our company through repeat and referral business achieved by customers
                        satisfaction in all areas including timeliness, attention to detail, and service-minded attitudes.
                        To develop & maintain the highest levels of Professionalism, Integrity, Honesty, and Fairness in our
                        relationship with our Suppliers, Subcontractors, Professional Associates
                    </p>
                    <hr>
                    <q>
                        <b>
                            If you want to work out what a safety displacement activity is, just take it out of the equation and see if it makes any difference. If there’s no difference then whatever that activity is, it’s probably a waste of time. - George Robotham
                        </b>
                    </q>
                    <hr>
                </div>
                <div class="col-md-4">
                    <img src="/frontend/assets/images/about/mission.png" alt="">
                </div>
            </div>
        </div>
    </section>
@section('scripts')
@endsection
@endsection
