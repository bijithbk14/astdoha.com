@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Shutdown projects Supports</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Shutdown projects Supports</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.service-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647607546/xh5eda8uiln6idwerz7y.png" alt="Shutdown projects Supports"></figure>
                        <div class="text">
                            <h2>Shutdown projects Supports</h2>
                            <p><b>Shut Down projects Division:</b> ATS provides Electrical, Mechanical, Technical and manpower support to plant and factory shutdown projects, Oil &amp; Gas, and Marine industry. ATS Shutdown support team have experience in repairs and maintenance of Engine, turbine, heat exchangers, pumps, valves, motors, generators, compressors, etc.</p>
                            <p>Our shutdown Projects manpower support is committed with qualified Engineers, Foremen, Draftsmen, Safety Officers, Supervisors, Marine Technicians, Valve Technicians, Pump Technicians Heat Exchangers Technicians, Turbocharger Specialist, Riggers, Plumbers, Duct Men, Steel Fixtures, Electricians, Pipe Fitters, Qualified Welders, Generator Technicians, Masons, Carpenters, Interior and Exterior Painters, Light and Heavy Duty Drivers, Cleaners etc.</p>
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            
                                            
                                            
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection