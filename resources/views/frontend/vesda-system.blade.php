@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>VESDA System</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>VESDA System</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647757843/h9dl2pazv2xvejqmtrfa.png" alt="VESDA System"></figure>
                        <div class="text">
                            <h2>VESDA System (Very Early Smoke Detection Apparatus)</h2>
                            <p>Advance Fire Protection System undertake Designs, Supplies, and Installation of VESDA system. It is an air sampling smoke detector system.</p><div>Where conventional systems have to be strategically placed to provide maximum protection, our VESDA systems can be installed in easily accessible positions as the system is complemented by a network of our high-grade ABS pipework.<br><div><br></div></div><div autocomplete="off">Additional benefits of using VESDA over conventional detectors are greater reliability and efficiency with features that include entire airflow analysis and ignition point targeting, with the ability to work alongside existing smoke detection and air conditioning systems symbiotically.<br><div><br></div></div><div>Our VESDA installations are in the widest variety of environments thanks to the ingenuity of the design, these including ceiling voids, cold rooms, computer rooms, and warehousing. The benefits and details of which can be viewed via the drop-down list above.<br></div><p></p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            
                                            
                                            
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection