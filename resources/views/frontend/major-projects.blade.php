@extends('frontend.base')
@section('content')
    <div class="boxed_wrapper">
        <section class="page-title centred"
            style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
            <div class="overlay-bg"></div>
            <div class="pattern-layer"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                        <h1>Major Projects</h1>
                    </div>
                    <ul class="bread-crumb clearfix">
                        <li><a href="index.html">Home</a></li>
                        <li autocomplete="off">Major Projects</li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="container mb-5">
            <ul class="nav nav-tabs justify-content-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link text-dark active" data-toggle="tab" href="#tab1" role="tab">
                        PROJECT ACCOMPLISHMENTS
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" data-toggle="tab" href="#tab2" role="tab">
                        ANNUAL PREVENT MAINTENANCE
                    </a>
                </li>
            </ul>
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content text-center">
                <div class="tab-pane active" id="tab1" role="tabpanel">
                    <table class="table table-bordered table-striped text-left">
                        <tbody>
                            <tr class="">
                                <td>
                                    <p><strong>#</strong></p>
                                </td>
                                <td>
                                    <p><strong>Project Name</strong></p>
                                </td>
                                <td>
                                    <p><strong>Client/main contractor</strong></p>
                                </td>
                                <td>
                                    <p><strong>Project Status</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>1</p>
                                </td>
                                <td>
                                    <p>Fire Fighting System Installation and commissioning in 5B+G+M+19+T Kahramaa
                                        Tower at Lussail</p>
                                </td>
                                <td>
                                    <p>Kahramaa</p>
                                </td>
                                <td>
                                    <p>Ongoing</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>2</p>
                                </td>
                                <td>
                                    <p>Supply, Installation, and Testing Commissioning of Firefighting &amp; Fire
                                        Alarm System in G+6 Movenpick hotel at Old Slata</p>
                                </td>
                                <td>
                                    <p>Movenpick</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>3</p>
                                </td>
                                <td>
                                    <p>Design, Supply, Installation &amp; Testing Commissioning of Novec 1230, VESDA
                                        system, Public Addressable System in Ooreddo New Expansion of QDC Zone 4
                                        &amp; Zone 5 </p>
                                </td>
                                <td>
                                    <p>Ooredoo</p>
                                </td>
                                <td>
                                    <p>Ongoing</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>4</p>
                                </td>
                                <td>
                                    <p>Supply, Installation &amp; Testing Commissioning of Fire-rated steel Door in
                                        Harrads Tea Shop at Hamad International Airport</p>
                                </td>
                                <td>
                                    <p> Shop@HIA</p>
                                </td>
                                <td>
                                    <p> Ongoing</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>5</p>
                                </td>
                                <td>
                                    <p>Design, Supply, Installation &amp; Testing Commissioning of FM 200 System At
                                        Twelve (12) Hanger and Workshop at New Tank Regiment camp, Dehailiyat,
                                        Shahaniya</p>
                                </td>
                                <td>
                                    <p>Qatar Armed Force</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>6</p>
                                </td>
                                <td>
                                    <p>Design, Supply, Installation &amp; Testing Commissioning of Ansul R102 Fire
                                        Suppression System for the Project Mess Hall at Shahaniya</p>
                                </td>
                                <td>
                                    <p> Qatar Armed Force</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>7</p>
                                </td>
                                <td>
                                    <p>Fire Fighting &amp; Fire Alarm System Up-gradation Work in B+G+19 Gulf Tower
                                        at &nbsp;West bay</p>
                                </td>
                                <td>
                                    <p> Ministry of Justice</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>8</p>
                                </td>
                                <td>
                                    <p>Supply, Installation &amp; Testing Commissioning of Fire Fighting &amp; Fire
                                        Alarm System in New Furniture Store, office &amp; Guardroom at Birket Al
                                        Awamer</p>
                                </td>
                                <td>
                                    <p>New Furniture Store</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>9</p>
                                </td>
                                <td>
                                    <p>Installation and Testing Commissioning of Firefighting &amp; Fire Alarm
                                        System in Qaffco Family Accommodation &amp; Guest 1 &amp; 2 Building at
                                        Mesaieed</p>
                                </td>
                                <td>
                                    <p> Qaffco</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>10</p>
                                </td>
                                <td>
                                    <p>Supply &amp; Installation of Fire Fighting and Fire Alarm System for the
                                        project Construction of 2 warehouses at Metro Projects</p>
                                </td>
                                <td>
                                    <p>Q Rail</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>11</p>
                                </td>
                                <td>
                                    <p>Design, Supply &amp; Installation of FM 200 System in Al Ahli Hospital at Bin
                                        Omran</p>
                                </td>
                                <td>
                                    <p>Al Ahli Hospital</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>12</p>
                                </td>
                                <td>
                                    <p>Design, Supply, Installation &amp; Testing Commissioning of Ansul R102 Fire
                                        Suppression System for the project Woqod Fuel Stations, Alkhor, Lusail &amp;
                                        Shamal City</p>
                                </td>
                                <td>
                                    <p>Woqod</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>13</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of Water Deluge System at Carrefour Gharaffa
                                    </p>
                                </td>
                                <td>
                                    <p>Carrefour</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>14</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of FM 200 System in 53 Rooms At beIN Sports
                                    </p>
                                </td>
                                <td>
                                    <p>beIN Sports</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>15</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of FM 200 and Fire Fighting system in Qatar
                                        Central Bank(QCB) Tower, Corniche-G+14 Building-Salam Tower</p>
                                </td>
                                <td>
                                    <p>Qatar Central Bank</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>16</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of FM 200 in QIB At City center</p>
                                </td>
                                <td>
                                    <p>QIB</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>17</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of FM 200 in Qatar Steel Mesaieed</p>
                                </td>
                                <td>
                                    <p>Qatar Steel</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>18</p>
                                </td>
                                <td>
                                    <p>Supply, Installation, and Testing Commissioning of Fire Alarm System in
                                        Workers Hospital @ Alkhor</p>
                                </td>
                                <td>
                                    <p>Workers Hospital</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>19</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of FM 200 in Carrefour Gharaffa</p>
                                </td>
                                <td>
                                    <p>Carrefour</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>20</p>
                                </td>
                                <td>
                                    <p>Servicing of Fire pump set with Replacement of Parts in Banana Island</p>
                                </td>
                                <td>
                                    <p>Banana Island</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>21</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of Fire stopping work for the GDI Project At
                                        Dukhan</p>
                                </td>
                                <td>
                                    <p>GDI Project</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>22</p>
                                </td>
                                <td>
                                    <p>Supply, Installation and Testing Commissioning of Firefighting &amp; Fire
                                        Alarm system in shop @ Mirqab Mall</p>
                                </td>
                                <td>
                                    <p>MirqabMall</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>23</p>
                                </td>
                                <td>
                                    <p>Supply, Installation, and Testing commissioning of Firefighting system in
                                        Keppel Seghers</p>
                                </td>
                                <td>
                                    <p>Keppel Seghers</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>24</p>
                                </td>
                                <td>
                                    <p>Supply, Installation, and Testing commissioning of Firefighting and Fire
                                        Alarm system in MDM Shop At Mall of Qatar</p>
                                </td>
                                <td>
                                    <p>MDM Shop</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>25</p>
                                </td>
                                <td>
                                    <p>Installation and Commissioning of Fire Alarm System in Watania School</p>
                                </td>
                                <td>
                                    <p>Watania School</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>26</p>
                                </td>
                                <td>
                                    <p>Design, Supply &amp; Installation of FM 200 System in Ministry of Economics
                                        &amp; Development, Corniche</p>
                                </td>
                                <td>
                                    <p>Ministry of Economics &amp; Development</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>27</p>
                                </td>
                                <td>
                                    <p>Design, Supply &amp; Installation of FM 200 System in CTC Qatar</p>
                                </td>
                                <td>
                                    <p>CTC Qatar</p>
                                </td>
                                <td>
                                    <p>Tested &amp; commissioned</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab2" role="tabpanel">
                    <table class="table table-bordered table-striped text-left">
                        <tbody>
                            <tr class="">
                                <td>
                                    <strong>#</strong>
                                </td>
                                <td>
                                    <p><strong>Project Name</strong></p>
                                </td>
                                <td>
                                    <p><strong>Client / Location</strong></p>
                                </td>
                                <td>
                                    <p><strong>Status</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>1</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, Fire Alarm, FM 200 system in
                                        Tunnel Projects in Mall of Qatar</p>
                                </td>
                                <td>
                                    <p>Ashgal Tunnel</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>2</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting and Fire Alarm System in
                                        Warehouse+ Office (G+M) at Raslaffan</p>
                                </td>
                                <td>
                                    <p>Raslaffan Industrial City</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>3</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, FM 200 &amp; Kitchen hood
                                        System - B+G+M+19 Hotel Building At Dafna</p>
                                </td>
                                <td>
                                    <p>3 Star hotel at Dafna</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>4</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, Fire Alarm, FM 200 system -
                                        3B+G+22+R Commercial building at Lusail</p>
                                </td>
                                <td>
                                    <p>Lusail</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>5</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting system in B+G+M+23 office
                                        Tower A &amp;B @ West bay</p>
                                </td>
                                <td>
                                    <p>Tower building @ West bay</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>6</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System, G+12
                                        Hotel building at Al salami street</p>
                                </td>
                                <td>
                                    <p>3 Star hotel At Al salami street</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>7</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System in
                                        B+G+7 building at Al Muntazah</p>
                                </td>
                                <td>
                                    <p>Residential Building At Al Muntaza</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>8</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting and Fire Alarm System in
                                        Office &amp; Warehouse at Mesaieed</p>
                                </td>
                                <td>
                                    <p>Mesaieed Industrial City</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>9</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting and Fire Alarm System in
                                        Warehouse building at Birket Al Awamer</p>
                                </td>
                                <td>
                                    <p>Wukair</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>10</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting and Fire Alarm System in Food
                                        Grain Bunker at Wukair</p>
                                </td>
                                <td>
                                    <p>Semi government entity, AL Wukair</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>11</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, Fire Alarm, Aerosol system in
                                        Large cinema theater.</p>
                                </td>
                                <td>
                                    <p>Mall @ Gharafa</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>12</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, Fire Alarm, FM 200 system in
                                        B+G+2 Supreme Council Building</p>
                                </td>
                                <td>
                                    <p>Najma</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>13</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, Fire Alarm, FM 200 system in
                                        B+G+2 Ehsan Building</p>
                                </td>
                                <td>
                                    <p>Najma</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>14</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System in
                                        Office + workshop building @ New industrial area</p>
                                </td>
                                <td>
                                    <p>New Industrial Area</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>15</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System in
                                        Accommodation and warehouse building @ Logistic Village</p>
                                </td>
                                <td>
                                    <p>Logistic Village</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>16</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System in G+1
                                        office, warehouse &amp; G+2 accommodation At Street No. 17</p>
                                </td>
                                <td>
                                    <p>Industrial area</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>17</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System in G+3
                                        building + mess hall At industrial area 25</p>
                                </td>
                                <td>
                                    <p>Industrial Area</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>18</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting &amp; Fire Alarm System in
                                        Office and Workshop At New Industrial area</p>
                                </td>
                                <td>
                                    <p>New Industrial Area</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>19</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire Fighting, Fire Alarm &amp; Kitchen hood
                                        System in Restaurant At City Center</p>
                                </td>
                                <td>
                                    <p>Restaurant At City center</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>20</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Firefighting, Fire alarm &amp; FM 200 System
                                        in Cement Factory At Umbab</p>
                                </td>
                                <td>
                                    <p>Cement Factory At Umbab</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>21</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire alarm System in Kindergarten school At
                                        Alkhor</p>
                                </td>
                                <td>
                                    <p>School Building At Alkhor</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>22</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Firefighting System in office building At
                                        Tornado Tower</p>
                                </td>
                                <td>
                                    <p>Office building At Tornado Tower</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>23</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Firefighting, Fire alarm &amp; FM 200 System
                                        in Head office building At Al Hilal</p>
                                </td>
                                <td>
                                    <p>Office building At Al Hilal</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>24</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Firefighting &amp; Fire alarm System in
                                        Jewellery Showrooms at Different Mall</p>
                                </td>
                                <td>
                                    <p>Jewelry showroom at various Locations</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>25</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Firefighting &amp; Fire alarm System in
                                        warehouse building At Street 34</p>
                                </td>
                                <td>
                                    <p>Industrial area</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>26</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire alarm System in Medical Center At Bin
                                        Omran</p>
                                </td>
                                <td>
                                    <p>Medical center At Bin Omran</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>27</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire alarm System in clinic building At
                                        Duhail, C ring &amp; Al waab</p>
                                </td>
                                <td>
                                    <p>Clinic At Duhail, Al waab &amp; C ring</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>28</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire alarm System in Kindergarten at Alkhor
                                    </p>
                                </td>
                                <td>
                                    <p>Kindergarten, Alchor</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>29</p>
                                </td>
                                <td>
                                    <p>Annual Preventive Maintenance of Fire alarm System in Kids skill training
                                        centers</p>
                                </td>
                                <td>
                                    <p>Kids Training Centers</p>
                                </td>
                                <td>
                                    <p>Active</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@section('scripts')
@endsection
@endsection
