@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Pre Action System</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Pre Action System</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647758757/cz16mygduj2ojayfnz6p.png" alt="Pre Action System"></figure>
                        <div class="text">
                            <h2>Pre Action System</h2>
                            <p></p><div><b>We, Advance Fire System do the installation, Repair, and Maintenance of Pre Action Fire system.</b></div><div>The system's discharge is a two-step process: First, the innovative detection system identifies smoke or heat, which activates a pre-action valve that allows water to flow into piping and effectively creates a wet pipe sprinkler system. Second, individual sprinkler heads release to let water flow onto the fire.</div><div autocomplete="off"><b>Deluge System – Water / Foam Deluge System</b></div><p></p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            
                                            
                                            
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection