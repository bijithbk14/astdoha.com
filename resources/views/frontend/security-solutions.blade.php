@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1><div>Security Solutions, Surveillance System &amp; CCTV</div></h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Security Solutions</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.service-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box" autocomplete="off">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647606072/l0ductz1oktfzyrpl9ru.png" alt="Security Solutions"></figure>
                        <div class="text">
                            <h2 autocomplete="off">Security Solutions, Surveillance System &amp; CCTV</h2>
                            <p><b>Security Solutions, Surveillance System &amp; CCTV: </b>ATS offers a complete range of customized security solutions for Access Control, Video Surveillance, IP Solutions, Intercoms, Alarm Monitoring, and Building Automations for Residential, Small Businesses, and Large Organizations.</p>

                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">

                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">


                                        <ul class="list clearfix">






                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




    @section('scripts')

@endsection
@endsection
