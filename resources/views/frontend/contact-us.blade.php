@extends('frontend.base')
@section('content')
    <section class="page-title centred" style="background-image: url('/frontend/assets/images/background/page-title.jpg');">
        <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
        <div class="auto-container">
            <div class="content-box">
                <div class="title">
                    <h1>Contact Us</h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="index.html">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="contact-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="content_block_6">
                        <div class="info-inner">
                            <h2>Drop us a Message</h2>
                            <ul class="info-list clearfix">
                                <li>
                                    <i class="flaticon-telephone"></i>
                                    <p><a href="tel:+974 7060 7079">+974 7060 7079</a>
                                    <br /><a href="tel:+974 4467 1539">+974 4467 1539</a>
                                    <br>
                                    Fax : +974 4464 5228
                                    </p>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <p><a href="mailto:info@atsdoha.com">info@atsdoha.com</a><br /><a
                                            href="mailto:sales@atsdoha.com">sales@atsdoha.com</a><br>
                                        <a href="mailto:atsdoha@gmail.com">atsdoha@gmail.com</a>
                                        </p>
                                </li>
                                <li>
                                    <i class="flaticon-pin"></i>
                                    <p>
                                        Advance Technical Services W.L.L
                                        <br>Office No: 5, Second Floor
                                        <br>Building No:133, Street No: 995
                                        <br>Abu Hamour, Doha
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-12 form-column" data-vvveb-disabled="">
                    <div class="form-inner">
                        <form method="post" action="/send-enquery" id="contact-form"
                            class="default-form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <input type="text" name="name" placeholder="Your Name *" required="">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <input type="email" name="email" placeholder="Your Email *" required="">
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                    <input type="text" name="phone" required="" placeholder="Your Phone">
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                    <input type="text" name="subject" required="" placeholder="Subject">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                    <textarea name="message" placeholder="Your Message ..."></textarea>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group message-btn">
                                    <button class="theme-btn-one" type="submit" name="submit-form">Submit comment</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact-section end -->


    <!-- google-map-two -->
    <section class="google-map-two" >
        <div class="map-inner">
            <div class="map-container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3609.121345146004!2d51.48347941501022!3d25.232837583879675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45c54fdc74e66d%3A0x71833df98c493ea1!2sADVANCE%20TECHNICAL%20SERVICES%20W.L.L-Advance%20Fire%20Protection%20System!5e0!3m2!1sen!2sin!4v1647269856059!5m2!1sen!2sin" width="100%" height="450" style="margin-bottom: -12px"></iframe>
            </div>
        </div>
    </section>
@section('scripts')
@endsection
@endsection
