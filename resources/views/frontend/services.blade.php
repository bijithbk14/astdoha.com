@extends('frontend.base')
@section('content')
        <section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
        <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
        <div class="auto-container">
            <div class="content-box">
                <div class="title">
                    <h1>Services</h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Services</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="service-page-section centred">
        <div class="auto-container">
            <div class="row clearfix">
                @if(count($services)>0)
                @foreach($services as $item)
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><img src="{{$item['thumbnail']}}" alt="{{$item['page_name']}}"></figure>
                            <div class="text">
                                <h2>{{$item['page_name']}}</h2>
                            </div>
                            <div class="overlay-content">
                                <div class="inner">
                                    <h3><a href="{{route('pages',$slug=$item['slug'])}}">{{$item['page_name']}}</a></h3>
                                    <p>There are many of pass ages of <br>available but the suffered.</p>
                                    <div class="link"><a href="{{route('pages',$slug=$item['slug'])}}">More</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-md-12">
                    <div class="sec-title text-center">
                        <h2>No Services Found</h2>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@section('scripts')
<script src="{{ asset('frontend/assets/js/owl.js')}}" class="additional-scripts-vvveb" remove-element-vvveb=""></script>
<script src="{{ asset('frontend/assets/js/wow.js')}}" class="additional-scripts-vvveb" remove-element-vvveb=""></script>

@endsection
@endsection
