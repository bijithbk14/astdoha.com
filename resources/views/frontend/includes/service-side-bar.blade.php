    <div class="service-sidebar default-sidebar">
        <div class="sidebar-widget categori-widget">
            <div class="widget-title">
                <h4>Services</h4>
            </div>
            <div class="widget-content">
                <ul class="categori-list clearfix">
                    @foreach ($services as $item)
                    <li><a href="{{route('pages',$slug=$item['slug'])}}" class="@if(Request::segment(2)==$item['slug']) active @endif">{{$item['page_name']}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="sidebar-widget advise-widget">
            <div class="inner-box centred" style="background-image: url(/frontend/assets/images/footer-logo.png);">
                <div class="icon-box"><i class="flaticon-life-saver"></i></div>
                <h2>ADVANCE TECHNICAL SERVICES W.L.L</h2>
            </div>
        </div>
    </div>
