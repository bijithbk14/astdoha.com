@extends('frontend.base')
@section('content')
    <section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
        <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
        <div class="auto-container">
            <div class="content-box">
                <div class="title">
                    <h1>Fire Fighting System</h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Fire Fighting System</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="service-details">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
                    @include('frontend.includes.fire-protection-side-bar')
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="service-details-content">
                        <div class="inner-box">
                            <figure class="image-box"><img
                                    src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647756593/kbboauteqq8uf31scadd.png"
                                    alt="Fire Fighting System"></figure>
                            <div class="text">
                                <h2>Fire Fighting System</h2>
                                <p><b>“The real enemy of safety is not non-compliance but non-thinking” - Dr. Rob Long</b>
                                </p>
                                <div autocomplete="off">
                                        Advance Fire Protection System designs, supplies, installs and services fire pumps,
                                        Sprinklers, Hydrants, Hose reels Fire water tanks which work to provide an adequate
                                        supply of water to the entire firefighting system
                                </div>
                                <br>
                                <div>Servicing of firefighting systems suitable for the protection of all risk and
                                    environment types. With fully trained designers, project managers, and engineers you can
                                    be assured of a professional and cost-effective solution</div>
                            </div>
                            <div class="two-column">
                                <div class="row align-items-center clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                        <div class="text">
                                            <ul class="list clearfix">
                                                <li>Fire Pumps and Fire Water Tanks</li>
                                                <li>Fire Sprinkler System</li>
                                                <li>Fire Hose Reel System</li>
                                                <li>Fire Hydrant System</li>
                                                <li>Foam and Water Deluge System</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>






@section('scripts')
@endsection
@endsection
