@extends('frontend.base')
@section('content')
    <section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
        <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
        <div class="auto-container">
            <div class="content-box">
                <div class="title">
                    <h1>Certifications</h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Certifications</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="service-page-section centred" data-vvveb-disabled="">
        <div class="auto-container">
            <div class="row clearfix">
                @if (count($certifications) != 0)
                    @foreach ($certifications as $item)
                        <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                            <div class="service-block-one wow fadeInUp animated animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="inner-box">
                                    <figure class="image-box"><img src="{{ $item['certificate'] }}" alt="{{ $item['description'] }}"></figure>
                                    <div class="text">
                                        <h2>{{ $item['description'] }}</h2>
                                    </div>
                                    <div class="overlay-content">
                                        <div class="inner">
                                            <h3><a href="">{{ $item['description'] }}</a></h3>
                                            <div class="link"><a href="{{ $item['certificate'] }}" class="lightbox-image" data-fancybox="gallery">View</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="sec-title text-center">
                            <h2>No Certificates Found</h2>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@section('scripts')
    <script src="{{ asset('frontend/assets/js/owl.js') }}" class="additional-scripts-vvveb" remove-element-vvveb="">
    </script>
    <script src="{{ asset('frontend/assets/js/wow.js') }}" class="additional-scripts-vvveb" remove-element-vvveb="">
    </script>
@endsection
@endsection
