@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>MEP Contracts Division</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>MEP Contracts Division</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.service-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647606669/npg5na2uvms7r0fe7suo.png" alt="MEP Contracts Division"></figure>
                        <div class="text">
                            <h2>MEP Contracts Division</h2>
                            <p></p><div><b>“ Delivering results, reliability, &amp; rock-solid dependability. ”</b></div><div>The Company’s MEP division undertakes and executes electrification and mechanical contracts include Fire Protection System, HVAC System includes maintenance of Air Conditioners, Chilled Water Piping, Ducting system, Ventilation System, etc, Plumbing &amp; Electrical, etc. We have skilled and experienced staff for electrical and mechanical design and engineering. “Advance” has developed capabilities for executing integrated MEP (Mechanical and Electrical) projects.</div><p></p>

                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">

                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">


                                        <ul class="list clearfix">






                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


    @section('scripts')

@endsection
@endsection
