@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Emergency &amp; Exit Lighting Systems</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Emergency &amp; Exit Lighting Systems</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647758572/ygcdfhfb5h7oi6wli5ar.png" alt="Emergency &amp; Exit Lighting Systems"></figure>
                        <div class="text">
                            <h2>Emergency &amp; Exit Lighting Systems</h2>
                            <p></p><div><b>We provide the design, supply, installation, and servicing of all emergency &amp; exit lighting.</b></div><div>The emergency lighting system provides illumination for the safety of people leaving a location. Exit Lighting/Escape route lighting – that part of an emergency escape lighting system provided to ensure that the means of escape can be effectively identified and safely used by occupants of the building.</div><p></p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            <li><div>Exit Light System</div></li>
                                            <li autocomplete="off">Emergency Light system</li>
                                            
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection