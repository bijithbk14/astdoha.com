@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Public Addressable / Voice Evacuation, Fire Telephone System</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Public Addressable / Voice Evacuation, Fire Telephone System</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.fire-protection-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647758419/jqtrhl3kuwwp4fmjs6yr.png" alt="Public Addressable / Voice Evacuation, Fire Telephone System"></figure>
                        <div class="text">
                            <h2>Public Addressable / Voice Evacuation, Fire Telephone System</h2>
                            <p></p><div>Advance Fire Protection System undertake Designs, Supplies, and Installation of Public Addressable System and Fire Telephone System.</div><div><br></div><div>A public address system (PA system) is an electronic sound amplification and distribution system with a microphone, amplifier, and loudspeakers, used to address a large public.</div><div><br></div><div>A Fire Telephone System is a form of EVCS (Emergency Voice Communication System) dedicated to the safe communication between Fire Officers and the building control point.</div><p></p>
                            
                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                    <div class="text">
                                        
                                        
                                        <ul class="list clearfix">
                                            
                                            
                                            
                                            
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
        
    @section('scripts')

@endsection
@endsection