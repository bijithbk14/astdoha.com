@extends('frontend.base')
@section('content')
<section class="page-title centred" style="background-image: url(/frontend/assets/images/background/page-title.jpg);">
    <div class="overlay-bg"></div>
        <div class="pattern-layer"></div>
             <div class="auto-container">
                <div class="content-box">
                    <div class="title">
                    <h1>Fire Protection System</h1>
                    </div>
                <ul class="bread-crumb clearfix">
                    <li><a href="/">Home</a></li>
                    <li>Fire Protection System</li>
                </ul>
             </div>
        </div>
    </section>
<section class="service-details">
    <div class="auto-container">
        <div class="row clearfix">
        <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side" data-vvveb-disabled="">
            @include('frontend.includes.service-side-bar')
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div class="inner-box">
                        <figure class="image-box"><img src="https://res.cloudinary.com/dxxlsebas/image/upload/v1647608494/yl3yrlbqgnnddc77o7dv.png" alt="Fire Protection System"></figure>
                        <div class="text">
                            <h2>Fire Protection System</h2>
                            <p></p><div autocomplete="off"><b>“ Be safe. Be aware. Fire dangers are present here. ”</b></div><div>Advance Fire Protection System offers a wide range of engineering solutions, Design, Supplies of materials, New Installations, Repair &amp; Maintenance, Upgrading, Testing &amp; commissioning of Fire Fighting, Fire Detection, and Fire Suppression system. Also, we offer the services required for Qatar Civil Defense Department (QCDD) such as Annual Maintenance Contracts (AMC), New Installations, Inspection &amp; Test Reports, Repair&amp; Maintenance, etc.</div>

                        </div>
                        <div class="two-column">
                            <div class="row align-items-center clearfix">

                                <div class="col-sm-12 content-column">
                                    <div class="text">
                                        <h3>Our hands on experience in the below sectors</h3>

                                        <ul class="list clearfix">
                                            <li>Fire Fighting System</li>
                                            <li>&nbsp;Fire Detection/Alarm System</li>
                                            <li>Public Addressable System</li>
                                            <li>Fire Telephone System</li>
                                            <li>Exit &amp; Emergency Lighting System</li>
                                            <li>Fire Suppression System (FM200, NIVAC, Wet chemical Kitchen Hood Suppression, NOVAC, etc)</li><li>Pre active system, Foam, and water deluge System</li><li autocomplete="off">&nbsp;Fire Extinguishers</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


            
        
    @section('scripts')

@endsection
@endsection