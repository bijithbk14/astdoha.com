@component('mail::message')
<h2>New Contact Information from - {{ $details['site'] }} </h2>
<p>Name - {{ $details['name'] }}</p>
<p>Email - {{ $details['email'] }}</p>
<p>Phone - {{ $details['phone'] }}</p>
<p>Subject - {{ $details['subject'] }}</p>
<p>Message - {{ $details['message'] }}</p>

Thanks,<br>
@endcomponent
