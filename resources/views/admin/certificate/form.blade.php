<div class="form-group{{ $errors->has('certificate') ? 'has-error' : '' }}">
    {!! Form::label('certificate', 'Certificate', ['class' => 'control-label']) !!}
    @if (isset($certificate) && $certificate->image != '')
        <div>
            <img onerror="this.onerror=null;this.src='https://res.cloudinary.com/dxxlsebas/image/upload/v1639911822/error_rwgafw.png';" src="{{ $certificate->image }}" width="50" height="50">
            <a href="{{ route('remove_certificate_image') }}?id={{ $certificate->id }}"
                onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger">delete</a>
        </div>
    @else
        {!! Form::file('certificate', 'required' == 'required' ? ['class' => 'form-control', 'required' => 'required', 'accept' => 'image/*'] : ['class' => 'form-control', 'accept' => 'image/*']) !!}
    @endif
    {!! $errors->first('certificate', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
