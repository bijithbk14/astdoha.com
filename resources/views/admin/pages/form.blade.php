<div class="form-group{{ $errors->has('template') ? 'has-error' : ''}}">
    {!! Form::label('template', 'Template', ['class' => 'control-label']) !!}
    {!! Form::number('template', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('template', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('meta_title') ? 'has-error' : ''}}">
    {!! Form::label('meta_title', 'Meta Title', ['class' => 'control-label']) !!}
    {!! Form::text('meta_title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('meta_description') ? 'has-error' : ''}}">
    {!! Form::label('meta_description', 'Meta Description', ['class' => 'control-label']) !!}
    {!! Form::text('meta_description', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('meta_keywords') ? 'has-error' : ''}}">
    {!! Form::label('meta_keywords', 'Meta Keywords', ['class' => 'control-label']) !!}
    {!! Form::text('meta_keywords', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('meta_keywords', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('page_name') ? 'has-error' : ''}}">
    {!! Form::label('page_name', 'Page Name', ['class' => 'control-label']) !!}
    {!! Form::text('page_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('page_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('file_name') ? 'has-error' : ''}}">
    {!! Form::label('file_name', 'File Name', ['class' => 'control-label']) !!}
    {!! Form::text('file_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('file_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('folder') ? 'has-error' : ''}}">
    {!! Form::label('folder', 'Folder', ['class' => 'control-label']) !!}
    {!! Form::text('folder', null,['class' => 'form-control']) !!}
    {!! $errors->first('folder', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
