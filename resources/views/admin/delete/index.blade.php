@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Delete Configuration</h6>
            </div>
            <div class="card-body">
                <div class="alert alert-warning">
                    Warning !. This method will delete the files(Controller,model,routes,migrations etc)
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                           @foreach ($menus as $item)
                               <tr>
                                   <td>{{$i++}}</td>
                                   <td>{{$item->title}}</td>
                                   <td><a href="{{ url('admin/delete-config') }}?name={{$item->title}}"
                                    onclick="confirm('Are you sure this will delete controller ,model, routes, migrations etc..')"
                                    class="btn btn-danger btn-sm"><i class="fa fa-trash"></a></td>
                               </tr>
                           @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
@endsection
