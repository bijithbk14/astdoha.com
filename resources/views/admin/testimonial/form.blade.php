<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('testimonial') ? 'has-error' : ''}}">
    {!! Form::label('testimonial', 'Testimonial', ['class' => 'control-label']) !!}
    {!! Form::textarea('testimonial', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('testimonial', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('who_is_who') ? 'has-error' : ''}}">
    {!! Form::label('who_is_who', 'Who Is Who', ['class' => 'control-label']) !!}
    {!! Form::text('who_is_who', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('who_is_who', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
