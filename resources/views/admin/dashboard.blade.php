@extends('layouts.backend')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>
<div class="row">
    <div class="card  mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Welcome </h6>
        </div>

        <div class="card-body">
            <div class="alert alert-warning">
                <b>Warning ! </b> please don't use the tools section
            </div>
            @foreach ($laravelAdminMenus->menus as $section)
                @if ($section->items)
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $section->section }}</b>
                        </div>

                        <div class="card-body row">
                                @foreach ($section->items as $menu)
                                    <div class="card  text-white col-md-2 ml-2 mb-2">
                                        <div class="card-body">
                                            <a class="nav-link text-dark" href="{{ url($menu->url) }}">
                                                {{ $menu->title }}
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                        </div>
                    </div>
                    <br />
                @endif
            @endforeach
        </div>
    </div>
</div>

</div>
<!-- /.container-fluid -->
@endsection
