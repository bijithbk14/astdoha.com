@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Contactu {{ $contactu->id }}</h6>
            </div>
            <div class="card-body">
                <a href="{{ url('/admin/contactus') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                            class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                {!! Form::open([
    'method' => 'DELETE',
    'url' => ['admin/contactus', $contactu->id],
    'style' => 'display:inline',
]) !!}
                {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> Delete', [
    'type' => 'submit',
    'class' => 'btn btn-danger btn-sm',
    'title' => 'Delete Contactu',
    'onclick' => 'return confirm("Confirm delete?")',
]) !!}
                {!! Form::close() !!}
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $contactu->id }}</td>
                            </tr>
                            <tr>
                                <th> Name </th>
                                <td> {{ $contactu->name }} </td>
                            </tr>
                            <tr>
                                <th> Email </th>
                                <td> {{ $contactu->email }} </td>
                            </tr>
                            <tr>
                                <th> Phone </th>
                                <td> {{ $contactu->phone }} </td>
                            </tr>
                            <tr>
                                <th> Subject </th>
                                <td> {{ $contactu->subject }} </td>
                            </tr>
                            <tr>
                                <th> Message </th>
                                <td> {{ $contactu->message }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
