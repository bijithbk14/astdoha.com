<div class="form-group{{ $errors->has('service_name') ? 'has-error' : ''}}">
    {!! Form::label('service_name', 'Service Name', ['class' => 'control-label']) !!}
    {!! Form::text('service_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('small_description') ? 'has-error' : ''}}">
    {!! Form::label('small_description', 'Small Description', ['class' => 'control-label']) !!}
    {!! Form::text('small_description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('small_description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'id'=>'editor1'] : ['class' => 'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image') ? 'has-error' : '' }}">
    {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
    @if (isset($service) && $service->image != '')
        <div>
            <img onerror="this.onerror=null;this.src='https://res.cloudinary.com/dxxlsebas/image/upload/v1639911822/error_rwgafw.png';" src="{{ $service->image }}" width="50" height="50">
            <a href="{{ route('remove_service_image') }}?id={{ $service->id }}"
                onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger">delete</a>
        </div>
    @else
        {!! Form::file('image', 'required' == 'required' ? ['class' => 'form-control', 'required' => 'required', 'accept' => 'image/*'] : ['class' => 'form-control', 'accept' => 'image/*']) !!}
    @endif
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
@section('scripts')
<script>
    // Replace the <textarea id="editor1"> with a CKEditor 4
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1' );
</script>
@endsection
