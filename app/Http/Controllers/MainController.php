<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Certificate;
use App\Models\Contactu;
use App\Models\Gallery;
use App\Models\Service;
use App\Models\Page;
use App\Models\ServiceDetail;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
class MainController extends Controller
{
    public function __construct(Request $request)
    {
        $meta_data['title'] = 'Advance Technical Services W.L.L';
        $meta_data['url'] = $request->url();
        $meta_data['description'] = 'Advance Technical Services W.L.L';
        $meta_data['image'] = '';
        $meta_data['keywords'] = '';
        View::share('meta_data', $meta_data);
        $menu = Page::where('parent',0)->orderBy('order', 'asc')->get();
        $menus =  [];
        foreach($menu as $item){
            $menu_data = ['menu' => $item->page_name,'slug'=>$item->slug, 'items' => []];
            $sub_menus = Page::where('parent',$item->id)->orderBy('order', 'asc')->get();
            foreach($sub_menus as $sub){
                $sub_menu = ['sub_menu'=>$sub->page_name,'sub_menu_slug'=>$sub->slug];
                array_push($menu_data['items'],$sub_menu);
            }
            array_push($menus,$menu_data);
        }
        View::share('menus', $menus);
    }
    public function index(Request $request){
        $page = Page::where('slug','home')->first();
        $file_name = str_replace('.blade.php','',$page->file_name);
        $testimonial = Testimonial::all()->toArray();
        $services =  $services =  Page::where('parent',11)->get();
        return view('frontend.'.$page->folder.'.'.$file_name,compact('page','testimonial','services'));
    }

    public function pages(Request $request){
        $gallery = array();
        $services = array();
        $certifications = array();
        $page = Page::where('slug',$request->slug)->first();
        $file_name = str_replace('.blade.php','',$page->file_name);
        if($page->slug=='gallery'){
           $gallery_category = Category::all();
           foreach($gallery_category as $item){
                $data = ['category'=>$item->category,'items'=>[]];
                $gallery_data =  Gallery::where('category',$item->id)->get();
                foreach($gallery_data as $item){
                    array_push($data['items'],['image'=>$item->image,'title'=>$item->title]);
                }
                array_push($gallery,$data);
           }
        }elseif($page->slug == 'certifications'){
            $certifications = Certificate::all()->toArray();
        }
        $services =  Page::where('parent',11)->get();
        $fire_protection_system = Page::where('parent',25)->get();
        $testimonial = Testimonial::all()->toArray();
        if($page->folder !=""){
            $data = array(
                'pages'=>'test',
                'gallery'=> $gallery,
                'services'=> $services,
                'fire_protection_system'=>$fire_protection_system,
                'page'=>$page,
                'testimonial'=>$testimonial,
                'certifications'=> $certifications
            );
            return view('frontend.'.$page->folder.'.'.$file_name)->with($data);
        }else{
            $pages = Page::all()->toArray();
            $data = array(
                'pages'=> $pages,
                'gallery'=> $gallery,
                'services'=> $services,
                'fire_protection_system'=>$fire_protection_system,
                'page'=>$page,
                'testimonial'=>$testimonial,
                'certifications'=> $certifications
            );
            return view('frontend.'.$page->folder.'.'.$file_name)->with($data);
        }

    }

    // public function service_details(Request $request){
    //     $gallery_category = array();
    //     $service = Service::where('id',$request->id)->first();
    //     $page = $service;
    //     foreach($details as $item){
    //         if(!in_array($item->title,$gallery_category)){
    //            array_push($gallery_category,$item->title);
    //         }
    //     }
    //     return view('frontend.service-details',compact('service','details','page','gallery_category'));
    // }

    function save_contact_enq(Request $request){

        $contact = new Contactu;
        $contact->name=$request->name;
        $contact->email=$request->email;
        $contact->phone=$request->phone;
        $contact->subject=$request->subject;
        $contact->message=$request->message;
        $contact->save();
        $details = [
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'subject'=>$request->subject,
            'message'=>$request->message,
        ];
        // Mail::to('info@tribunets.com')->send(new \App\Mail\ContactForm($details));
        $data = array('result'=>'success','message'=>'Thankyou for contacting us!');
        return redirect(route('index'));
        //return json_encode($data);
    }
}
