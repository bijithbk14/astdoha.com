<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Certificate;
use Illuminate\Http\Request;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $certificate = Certificate::where('certificate', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $certificate = Certificate::latest()->paginate($perPage);
        }

        return view('admin.certificate.index', compact('certificate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'certificate' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
			'description' => 'required'
		]);
        $requestData = $request->all();
        if($request->certificate){
            $certificate = Cloudinary::upload($request->file('certificate')->getRealPath())->getSecurePath();
            $requestData['certificate'] = $certificate;
        }
        Certificate::create($requestData);

        return redirect('admin/certificate')->with('flash_message', 'Certificate added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $certificate = Certificate::findOrFail($id);

        return view('admin.certificate.show', compact('certificate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $certificate = Certificate::findOrFail($id);

        return view('admin.certificate.edit', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'description' => 'required'
		]);
        $requestData = $request->all();
        if($request->certificate){
            $certificate = Cloudinary::upload($request->file('certificate')->getRealPath())->getSecurePath();
            $requestData['certificate'] = $certificate;
        }
        $certificate = Certificate::findOrFail($id);
        $certificate->update($requestData);

        return redirect('admin/certificate')->with('flash_message', 'Certificate updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Certificate::destroy($id);

        return redirect('admin/certificate')->with('flash_message', 'Certificate deleted!');
    }

    public function delete_certificate_image(Request $request){
        $course = Certificate::findOrFail($request->id);
        $course->update(array('certificate'=>''));
        return redirect()->back();
    }
}
