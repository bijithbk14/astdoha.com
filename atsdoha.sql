-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: atsdoha
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint unsigned DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint unsigned DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subject` (`subject_type`,`subject_id`),
  KEY `causer` (`causer_type`,`causer_id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'default','App\\Models\\Page model has been created','App\\Models\\Page',4,'App\\Models\\User',1,'[]','2022-03-14 20:44:30','2022-03-14 20:44:30'),(2,'default','App\\Models\\Service model has been created','App\\Models\\Service',1,'App\\Models\\User',1,'[]','2022-03-14 22:41:34','2022-03-14 22:41:34'),(3,'default','App\\Models\\Service model has been created','App\\Models\\Service',2,'App\\Models\\User',1,'[]','2022-03-15 00:22:48','2022-03-15 00:22:48'),(4,'default','App\\Models\\Service model has been updated','App\\Models\\Service',1,'App\\Models\\User',1,'[]','2022-03-15 00:22:55','2022-03-15 00:22:55'),(5,'default','App\\Models\\Service model has been created','App\\Models\\Service',3,'App\\Models\\User',1,'[]','2022-03-15 00:48:21','2022-03-15 00:48:21'),(6,'default','App\\Models\\Service model has been created','App\\Models\\Service',4,'App\\Models\\User',1,'[]','2022-03-15 00:49:26','2022-03-15 00:49:26'),(7,'default','App\\Models\\Service model has been created','App\\Models\\Service',5,'App\\Models\\User',1,'[]','2022-03-15 00:50:12','2022-03-15 00:50:12'),(8,'default','App\\Models\\Service model has been created','App\\Models\\Service',6,'App\\Models\\User',1,'[]','2022-03-15 00:50:50','2022-03-15 00:50:50'),(9,'default','App\\Models\\Service model has been created','App\\Models\\Service',7,'App\\Models\\User',1,'[]','2022-03-15 00:51:30','2022-03-15 00:51:30'),(10,'default','App\\Models\\Page model has been created','App\\Models\\Page',5,'App\\Models\\User',1,'[]','2022-03-16 02:15:23','2022-03-16 02:15:23'),(11,'default','App\\Models\\Certificate model has been created','App\\Models\\Certificate',1,'App\\Models\\User',1,'[]','2022-03-16 05:26:02','2022-03-16 05:26:02'),(12,'default','App\\Models\\Certificate model has been created','App\\Models\\Certificate',2,'App\\Models\\User',1,'[]','2022-03-16 05:26:17','2022-03-16 05:26:17'),(13,'default','App\\Models\\Certificate model has been created','App\\Models\\Certificate',3,'App\\Models\\User',1,'[]','2022-03-16 05:26:28','2022-03-16 05:26:28'),(14,'default','App\\Models\\Certificate model has been created','App\\Models\\Certificate',4,'App\\Models\\User',1,'[]','2022-03-16 05:26:48','2022-03-16 05:26:48'),(15,'default','App\\Models\\Certificate model has been created','App\\Models\\Certificate',5,'App\\Models\\User',1,'[]','2022-03-16 05:27:01','2022-03-16 05:27:01'),(16,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',5,'App\\Models\\User',1,'[]','2022-03-16 05:41:20','2022-03-16 05:41:20'),(17,'default','App\\Models\\Page model has been created','App\\Models\\Page',6,'App\\Models\\User',1,'[]','2022-03-16 05:42:30','2022-03-16 05:42:30'),(18,'default','App\\Models\\Page model has been created','App\\Models\\Page',7,'App\\Models\\User',1,'[]','2022-03-16 05:42:58','2022-03-16 05:42:58'),(19,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',7,'App\\Models\\User',1,'[]','2022-03-16 05:43:17','2022-03-16 05:43:17'),(20,'default','App\\Models\\Page model has been created','App\\Models\\Page',8,'App\\Models\\User',1,'[]','2022-03-16 05:44:22','2022-03-16 05:44:22'),(21,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',8,'App\\Models\\User',1,'[]','2022-03-16 05:46:41','2022-03-16 05:46:41'),(22,'default','App\\Models\\Page model has been created','App\\Models\\Page',9,'App\\Models\\User',1,'[]','2022-03-16 05:48:59','2022-03-16 05:48:59'),(23,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',9,'App\\Models\\User',1,'[]','2022-03-16 05:49:09','2022-03-16 05:49:09'),(24,'default','App\\Models\\Page model has been created','App\\Models\\Page',10,'App\\Models\\User',1,'[]','2022-03-16 05:49:56','2022-03-16 05:49:56'),(25,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',10,'App\\Models\\User',1,'[]','2022-03-16 05:50:28','2022-03-16 05:50:28'),(26,'default','App\\Models\\Page model has been created','App\\Models\\Page',11,'App\\Models\\User',1,'[]','2022-03-17 03:55:07','2022-03-17 03:55:07'),(27,'default','App\\Models\\Page model has been created','App\\Models\\Page',12,'App\\Models\\User',1,'[]','2022-03-17 04:06:10','2022-03-17 04:06:10'),(28,'default','App\\Models\\Cloudinary model has been created','App\\Models\\Cloudinary',1,'App\\Models\\User',1,'[]','2022-03-17 07:22:00','2022-03-17 07:22:00'),(29,'default','App\\Models\\Page model has been created','App\\Models\\Page',13,'App\\Models\\User',1,'[]','2022-03-17 09:18:44','2022-03-17 09:18:44'),(30,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',13,'App\\Models\\User',1,'[]','2022-03-17 09:25:30','2022-03-17 09:25:30'),(31,'default','App\\Models\\Page model has been created','App\\Models\\Page',14,'App\\Models\\User',1,'[]','2022-03-17 09:26:13','2022-03-17 09:26:13'),(32,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',14,'App\\Models\\User',1,'[]','2022-03-17 09:26:40','2022-03-17 09:26:40'),(33,'default','App\\Models\\Page model has been created','App\\Models\\Page',15,'App\\Models\\User',1,'[]','2022-03-17 09:27:02','2022-03-17 09:27:02'),(34,'default','App\\Models\\Cloudinary model has been created','App\\Models\\Cloudinary',2,'App\\Models\\User',1,'[]','2022-03-17 09:38:49','2022-03-17 09:38:49'),(35,'default','App\\Models\\Page model has been created','App\\Models\\Page',16,'App\\Models\\User',1,'[]','2022-03-17 22:16:44','2022-03-17 22:16:44'),(36,'default','App\\Models\\Page model has been created','App\\Models\\Page',17,'App\\Models\\User',1,'[]','2022-03-17 22:18:21','2022-03-17 22:18:21'),(37,'default','App\\Models\\Category model has been created','App\\Models\\Category',1,'App\\Models\\User',1,'[]','2022-03-17 22:46:00','2022-03-17 22:46:00'),(38,'default','App\\Models\\Category model has been created','App\\Models\\Category',2,'App\\Models\\User',1,'[]','2022-03-17 22:57:05','2022-03-17 22:57:05'),(39,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',1,'App\\Models\\User',1,'[]','2022-03-17 23:04:21','2022-03-17 23:04:21'),(40,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',2,'App\\Models\\User',1,'[]','2022-03-17 23:04:45','2022-03-17 23:04:45'),(41,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',3,'App\\Models\\User',1,'[]','2022-03-17 23:05:00','2022-03-17 23:05:00'),(42,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',4,'App\\Models\\User',1,'[]','2022-03-17 23:05:14','2022-03-17 23:05:14'),(43,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',5,'App\\Models\\User',1,'[]','2022-03-17 23:05:29','2022-03-17 23:05:29'),(44,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',6,'App\\Models\\User',1,'[]','2022-03-17 23:07:43','2022-03-17 23:07:43'),(45,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',7,'App\\Models\\User',1,'[]','2022-03-17 23:08:01','2022-03-17 23:08:01'),(46,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',8,'App\\Models\\User',1,'[]','2022-03-17 23:08:35','2022-03-17 23:08:35'),(47,'default','App\\Models\\Gallery model has been created','App\\Models\\Gallery',9,'App\\Models\\User',1,'[]','2022-03-17 23:08:48','2022-03-17 23:08:48'),(48,'default','App\\Models\\Page model has been created','App\\Models\\Page',18,'App\\Models\\User',1,'[]','2022-03-17 23:09:59','2022-03-17 23:09:59'),(49,'default','App\\Models\\Page model has been created','App\\Models\\Page',19,'App\\Models\\User',1,'[]','2022-03-18 06:51:13','2022-03-18 06:51:13'),(50,'default','App\\Models\\Page model has been created','App\\Models\\Page',20,'App\\Models\\User',1,'[]','2022-03-18 07:01:09','2022-03-18 07:01:09'),(51,'default','App\\Models\\Page model has been created','App\\Models\\Page',21,'App\\Models\\User',1,'[]','2022-03-18 07:07:29','2022-03-18 07:07:29'),(52,'default','App\\Models\\Page model has been created','App\\Models\\Page',22,'App\\Models\\User',1,'[]','2022-03-18 07:15:46','2022-03-18 07:15:46'),(53,'default','App\\Models\\Page model has been created','App\\Models\\Page',23,'App\\Models\\User',1,'[]','2022-03-18 07:20:20','2022-03-18 07:20:20'),(54,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',12,'App\\Models\\User',1,'[]','2022-03-18 07:30:37','2022-03-18 07:30:37'),(55,'default','App\\Models\\Page model has been created','App\\Models\\Page',24,'App\\Models\\User',1,'[]','2022-03-18 07:31:35','2022-03-18 07:31:35'),(56,'default','App\\Models\\Page model has been created','App\\Models\\Page',25,'App\\Models\\User',1,'[]','2022-03-20 00:33:25','2022-03-20 00:33:25'),(57,'default','App\\Models\\Page model has been created','App\\Models\\Page',26,'App\\Models\\User',1,'[]','2022-03-20 00:39:52','2022-03-20 00:39:52'),(58,'default','App\\Models\\Page model has been created','App\\Models\\Page',27,'App\\Models\\User',1,'[]','2022-03-20 00:57:00','2022-03-20 00:57:00'),(59,'default','App\\Models\\Page model has been created','App\\Models\\Page',28,'App\\Models\\User',1,'[]','2022-03-20 01:00:43','2022-03-20 01:00:43'),(60,'default','App\\Models\\Page model has been created','App\\Models\\Page',29,'App\\Models\\User',1,'[]','2022-03-20 01:04:48','2022-03-20 01:04:48'),(61,'default','App\\Models\\Page model has been created','App\\Models\\Page',30,'App\\Models\\User',1,'[]','2022-03-20 01:07:29','2022-03-20 01:07:29'),(62,'default','App\\Models\\Page model has been deleted','App\\Models\\Page',30,'App\\Models\\User',1,'[]','2022-03-20 01:08:06','2022-03-20 01:08:06'),(63,'default','App\\Models\\Page model has been created','App\\Models\\Page',31,'App\\Models\\User',1,'[]','2022-03-20 01:10:19','2022-03-20 01:10:19'),(64,'default','App\\Models\\Page model has been created','App\\Models\\Page',32,'App\\Models\\User',1,'[]','2022-03-20 01:12:52','2022-03-20 01:12:52'),(65,'default','App\\Models\\Page model has been created','App\\Models\\Page',33,'App\\Models\\User',1,'[]','2022-03-20 01:15:57','2022-03-20 01:15:57'),(66,'default','App\\Models\\Page model has been created','App\\Models\\Page',34,'App\\Models\\User',1,'[]','2022-03-20 01:18:55','2022-03-20 01:18:55'),(67,'default','App\\Models\\Page model has been created','App\\Models\\Page',35,'App\\Models\\User',1,'[]','2022-03-20 01:23:01','2022-03-20 01:23:01'),(68,'default','App\\Models\\Contactu model has been created','App\\Models\\Contactu',1,'App\\Models\\User',1,'[]','2022-03-20 01:54:46','2022-03-20 01:54:46'),(69,'default','App\\Models\\Contactu model has been created','App\\Models\\Contactu',2,'App\\Models\\User',1,'[]','2022-03-20 01:58:54','2022-03-20 01:58:54'),(70,'default','App\\Models\\Contactu model has been created','App\\Models\\Contactu',3,'App\\Models\\User',1,'[]','2022-03-20 01:59:47','2022-03-20 01:59:47');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'2022-03-17 22:46:00','2022-03-17 22:46:00','Milipol Qatar 2018- 12th International Exhibition of Home Land Security Incorporate with Qatar Civil Defence Department'),(2,'2022-03-17 22:57:05','2022-03-17 22:57:05','5th Qatar Civil Defense Exhibition & Conference at Doha Exhibition Centre-2015 Nov');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certificates` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT INTO `certificates` VALUES (1,'2022-03-16 05:26:02','2022-03-16 05:26:02','https://res.cloudinary.com/dxxlsebas/image/upload/v1647428162/g3yqfgak0wantngpfnu0.png','Certificate of Participation - Civil Defence Exhibition & Conference'),(2,'2022-03-16 05:26:17','2022-03-16 05:26:17','https://res.cloudinary.com/dxxlsebas/image/upload/v1647428177/vplrh3ri9v3r8wkean3f.png','Certificate of Participation - Milipol Qatar 2018'),(3,'2022-03-16 05:26:28','2022-03-16 05:26:28','https://res.cloudinary.com/dxxlsebas/image/upload/v1647428187/gcwnudjsueqqjprtxgn1.png','Certificate - ISO 9001: 2015'),(4,'2022-03-16 05:26:48','2022-03-16 05:26:48','https://res.cloudinary.com/dxxlsebas/image/upload/v1647428207/nbsydzo9mfwp1mvubazh.png','Certificate - ISO 9001: 2015'),(5,'2022-03-16 05:27:01','2022-03-16 05:27:01','https://res.cloudinary.com/dxxlsebas/image/upload/v1647428220/zzzv9tlr4pbpxbu8twlx.png','Certificate - ISO 9001: 2015');
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cloudinaries`
--

DROP TABLE IF EXISTS `cloudinaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cloudinaries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cloudinaries`
--

LOCK TABLES `cloudinaries` WRITE;
/*!40000 ALTER TABLE `cloudinaries` DISABLE KEYS */;
INSERT INTO `cloudinaries` VALUES (1,'2022-03-17 07:22:00','2022-03-17 07:22:00','https://res.cloudinary.com/dxxlsebas/image/upload/v1647521519/uuoekfxl4cdzrymgxkec.png'),(2,'2022-03-17 09:38:49','2022-03-17 09:38:49','https://res.cloudinary.com/dxxlsebas/image/upload/v1647529728/ghid4gp7hj2ij41dzz85.png');
/*!40000 ALTER TABLE `cloudinaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactuses`
--

DROP TABLE IF EXISTS `contactuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contactuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactuses`
--

LOCK TABLES `contactuses` WRITE;
/*!40000 ALTER TABLE `contactuses` DISABLE KEYS */;
INSERT INTO `contactuses` VALUES (1,'2022-03-20 01:54:46','2022-03-20 01:54:46','bijith','bijith@gmail.com','9744567987','test','tsetse'),(2,'2022-03-20 01:58:54','2022-03-20 01:58:54','bijith','bijith@gmmm.com','4343342344','43243','fdsfdsfsdfsdf'),(3,'2022-03-20 01:59:47','2022-03-20 01:59:47','tetse','fdsfsfsf@gmail.com','vdfdfdf','dfdfd','fdfdfd');
/*!40000 ALTER TABLE `contactuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galleries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'2022-03-17 23:04:21','2022-03-17 23:04:21','Milipol Qatar 2018- 12th International Exhibition','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578060/x3e7wmktpubwf2vygcud.png',NULL,1),(2,'2022-03-17 23:04:45','2022-03-17 23:04:45','Milipol Qatar 2018- 12th International Exhibition','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578084/bjx9fkhgkkhaj7xellt2.png',NULL,1),(3,'2022-03-17 23:05:00','2022-03-17 23:05:00','Milipol Qatar 2018- 12th International Exhibition','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578099/egd0hsywh7kgv4wlx3um.png',NULL,1),(4,'2022-03-17 23:05:14','2022-03-17 23:05:14','Milipol Qatar 2018- 12th International Exhibition','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578113/khdwyuurbrymmku4xoeo.png',NULL,1),(5,'2022-03-17 23:05:29','2022-03-17 23:05:29','Milipol Qatar 2018- 12th International Exhibition','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578128/zd7rve5ntohdsinwgig4.png',NULL,1),(6,'2022-03-17 23:07:43','2022-03-17 23:07:43','5th Qatar Civil Defense Exhibition & Conference','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578262/nexkcgpxb5lrqtmudqku.png',NULL,2),(7,'2022-03-17 23:08:01','2022-03-17 23:08:01','5th Qatar Civil Defense Exhibition & Conference','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578280/cbb72gbxfsuunmm5v8du.png',NULL,2),(8,'2022-03-17 23:08:35','2022-03-17 23:08:35','5th Qatar Civil Defense Exhibition & Conference','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578314/ja9igoivd3wqin3oanxv.png',NULL,2),(9,'2022-03-17 23:08:48','2022-03-17 23:08:48','5th Qatar Civil Defense Exhibition & Conference','https://res.cloudinary.com/dxxlsebas/image/upload/v1647578327/wzdonipreq0tfwzjhnho.png',NULL,2);
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_193651_create_roles_permissions_tables',1),(4,'2018_08_04_122319_create_settings_table',1),(5,'2019_08_19_000000_create_failed_jobs_table',1),(6,'2019_12_14_000001_create_personal_access_tokens_table',1),(7,'2021_12_10_121031_create_activity_log_table',1),(8,'2021_12_31_042153_create_pages_table',1),(9,'2022_01_08_122629_create_galleries_table',1),(10,'2022_01_24_114242_create_contactuses_table',1),(11,'2022_03_03_151636_create_services_table',1),(12,'2022_03_05_035317_create_categories_table',1),(13,'2022_03_06_015415_create_service_details_table',1),(14,'2022_03_10_024554_create_testimonials_table',1),(15,'2022_03_15_040331_create_services_table',2),(16,'2022_03_16_104814_create_certificates_table',3),(17,'2022_03_18_041515_create_categories_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `template` int DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_edit` int DEFAULT NULL,
  `order` int DEFAULT NULL,
  `parent` int DEFAULT '0',
  `thumbnail` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'2022-03-14 20:44:30','2022-03-14 20:44:30',1,'Home','Home','Home','Home','home.blade.php',NULL,'home',1,1,0,NULL),(2,'2022-03-14 20:44:30','2022-03-14 20:44:30',1,'About Us','About Us','About Us','About Us','about-us.blade.php',NULL,'about-us',1,2,0,NULL),(3,'2022-03-14 20:44:30','2022-03-14 20:44:30',1,'Contact Us','Contact Us','Contact Us','Contact Us','contact-us.blade.php',NULL,'contact-us',1,6,0,NULL),(4,'2022-03-14 20:44:30','2022-03-14 20:44:30',1,'Major Projects','Major Projects','Major Projects','Major Projects','major-projects.blade.php',NULL,'major-projects',1,4,16,NULL),(11,'2022-03-17 03:55:07','2022-03-17 03:55:07',1,'Services','Services','Services','Services','services.blade.php',NULL,'services',0,3,0,NULL),(15,'2022-03-17 09:27:02','2022-03-17 09:27:02',2,'Safety Supports','Safety Supports','Safety Supports','Safety Supports','safety-supports.blade.php',NULL,'safety-supports',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647529022/emgxcqrry1c2ecxk59hp.png'),(16,'2022-03-17 22:16:44','2022-03-17 22:16:44',1,'More','More','More','More + ','more.blade.php',NULL,'more',1,7,0,NULL),(17,'2022-03-17 22:18:21','2022-03-17 22:18:21',1,'Certifications','Certifications','Certifications','Certifications','certifications.blade.php',NULL,'certifications',1,NULL,16,NULL),(18,'2022-03-17 23:09:59','2022-03-17 23:09:59',1,'Gallery','Gallery','Gallery','Gallery','gallery.blade.php',NULL,'gallery',1,5,0,NULL),(19,'2022-03-18 06:51:13','2022-03-18 06:51:13',2,'Security Solutions','FIRE PROTECTION SYSTEM\r\nSECURITY SOLUTIONS\r\nMEP CONTRACTING\r\nHVAC PROJECTS\r\nSAFETY SUPPORTS\r\nSHUTDOWN PROJECT SUPPORT\r\nTRADING\r\nSecurity Solutions, Surveillance System & CCTV','Security Solutions','Security Solutions','security-solutions.blade.php',NULL,'security-solutions',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647606072/l0ductz1oktfzyrpl9ru.png'),(20,'2022-03-18 07:01:09','2022-03-18 07:01:09',2,'MEP Contracts Division','MEP Contracts Division','MEP Contracts Division','MEP Contracts Division','mep-contracts-division.blade.php',NULL,'mep-contracts-division',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647606669/npg5na2uvms7r0fe7suo.png'),(21,'2022-03-18 07:07:29','2022-03-18 07:07:29',2,'HVAC Projects Division','HVAC Projects Division','HVAC Projects Division','HVAC Projects Division','hvac-projects-division.blade.php',NULL,'hvac-projects-division',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647607049/nigu6kwcws8msw1on8wn.png'),(22,'2022-03-18 07:15:46','2022-03-18 07:15:46',2,'Shutdown projects Supports','Shutdown projects Supports','Shutdown projects Supports','Shutdown projects Supports','shutdown-projects-supports.blade.php',NULL,'shutdown-projects-supports',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647607546/xh5eda8uiln6idwerz7y.png'),(23,'2022-03-18 07:20:20','2022-03-18 07:20:20',2,'Trading','Trading','Trading','Trading','trading.blade.php',NULL,'trading',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647607819/slkcgtqtkfc6bhz4j006.png'),(24,'2022-03-18 07:31:35','2022-03-18 07:31:35',2,'Fire Protection System','Fire Protection System','Fire Protection System','Fire Protection System','fire-protection-system.blade.php',NULL,'fire-protection-system',1,NULL,11,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647608494/yl3yrlbqgnnddc77o7dv.png'),(25,'2022-03-20 00:33:25','2022-03-20 00:33:25',2,'Fire Protection System','Fire Protection System','Fire Protection System','Fire Protection System','fire-protection-system-base.blade.php',NULL,'fire-protection-system',1,3,0,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647756207/s5pr93n5n7zab62g2byh.png'),(26,'2022-03-20 00:39:52','2022-03-20 00:39:52',2,'Fire Fighting System','Fire Fighting System','Fire Fighting System','Fire Fighting System','fire-fighting-system.blade.php',NULL,'fire-fighting-system',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647756593/kbboauteqq8uf31scadd.png'),(27,'2022-03-20 00:57:00','2022-03-20 00:57:00',3,'Fire Detection / Alarm System','Fire Detection / Alarm System','Fire Detection / Alarm System','Fire Detection / Alarm System','fire-detection--alarm-system.blade.php',NULL,'fire-detection-alarm-system',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647757621/qydrkyiazcwuf7jbyyyt.png'),(28,'2022-03-20 01:00:43','2022-03-20 01:00:43',3,'VESDA System','VESDA System','VESDA System','VESDA System','vesda-system.blade.php',NULL,'vesda-system',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647757843/h9dl2pazv2xvejqmtrfa.png'),(29,'2022-03-20 01:04:48','2022-03-20 01:04:48',3,'Fire Suppression System','Fire Suppression System','Fire Suppression System','Fire Suppression System','fire-suppression-system.blade.php',NULL,'fire-suppression-system',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647758087/sjxgll85x0lmolgnjj4i.png'),(31,'2022-03-20 01:10:19','2022-03-20 01:10:19',3,'Public Addressable / Voice Evacuation, Fire Telephone System','Public Addressable / Voice Evacuation, Fire Telephone System','Public Addressable / Voice Evacuation, Fire Telephone System','Public Addressable / Voice Evacuation, Fire Telephone System','public-addressable--voice-evacuation-fire-telephone-system.blade.php',NULL,'public-addressable-voice-evacuation-fire-telephone-system',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647758419/jqtrhl3kuwwp4fmjs6yr.png'),(32,'2022-03-20 01:12:52','2022-03-20 01:12:52',3,'Emergency & Exit Lighting Systems','Emergency & Exit Lighting Systems','Emergency & Exit Lighting Systems','Emergency & Exit Lighting Systems','emergency-exit-lighting-systems.blade.php',NULL,'emergency-exit-lighting-systems',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647758572/ygcdfhfb5h7oi6wli5ar.png'),(33,'2022-03-20 01:15:57','2022-03-20 01:15:57',3,'Pre Action System','Pre Action System','Pre Action System','Pre Action System','pre-action-system.blade.php',NULL,'pre-action-system',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647758757/cz16mygduj2ojayfnz6p.png'),(34,'2022-03-20 01:18:55','2022-03-20 01:18:55',3,'Passive Fire Protections','Passive Fire Protections','Passive Fire Protections','Passive Fire Protections','passive-fire-protections.blade.php',NULL,'passive-fire-protections',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647758935/g6l2f02mm8egwowwfztf.png'),(35,'2022-03-20 01:23:01','2022-03-20 01:23:01',3,'Fire Extinguishers','Fire Extinguishers','Fire Extinguishers','Fire Extinguishers','fire-extinguishers.blade.php',NULL,'fire-extinguishers',1,NULL,25,'https://res.cloudinary.com/dxxlsebas/image/upload/v1647759180/x8b5g6am03xkpocggpso.png');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user` (
  `role_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `small_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'2022-03-14 22:41:34','2022-03-15 00:22:55',NULL,'Fire Protection System','Where Children Are Taught, Fires Will Be Naught.','<p>Advance Fire Protection System offers a wide range of engineering solutions, Design, Supplies of materials, New Installations, Repair &amp; Maintenance, Upgrading, Testing &amp; commissioning of Fire Fighting, Fire Detection, and Fire Suppression system. Also, we offer the services required for Qatar Civil Defense Department (QCDD) such as Annual Maintenance Contracts (AMC), New Installations, Inspection &amp; Test Reports, Repair&amp; Maintenance, etc. We deliver a full range of fire protection solutions to businesses and individuals, which are designed to be cost-effective at all times. With a speedy response service, the business also has polite and friendly, experienced personnel enabling clients to understand and comply with current regulations.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647317493/bzrh61cakqcuzzekksc4.png','fire-protection-system'),(2,'2022-03-15 00:22:48','2022-03-15 00:22:48',NULL,'Security Solutions, Surveillance System & CCTV','Trust us with your business, We will guard you with ours.','<p><strong>Security Solutions, Surveillance System &amp; CCTV:&nbsp;</strong>ATS offers a complete range of customized security solutions for Access Control, Video Surveillance, IP Solutions, Intercoms, Alarm Monitoring, and Building Automations for Residential, Small Businesses, and Large Organizations.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647323567/j7dbbsmfovbyrkovaumg.png','security-solutions-surveillance-system-cctv'),(3,'2022-03-15 00:48:21','2022-03-15 00:48:21',NULL,'MEP Contracts Division','Delivering results, reliability, & rock-solid dependability.','<p>The Company&rsquo;s MEP division undertakes and executes electrification and mechanical contracts include Fire Protection System, HVAC System includes maintenance of Air Conditioners, Chilled Water Piping, Ducting system, Ventilation System, etc, Plumbing &amp; Electrical, etc. We have skilled and experienced staff for electrical and mechanical design and engineering. &ldquo;Advance&rdquo; has developed capabilities for executing integrated MEP (Mechanical and Electrical) projects.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647325100/yuxoxfbzhzlnaqztym5c.png','mep-contracts-division'),(4,'2022-03-15 00:49:26','2022-03-15 00:49:26',NULL,'HVAC Projects Division',NULL,'<p>⥤ &nbsp;&nbsp;<strong>Chilled Water Piping</strong></p>\r\n\r\n<p>Chilled water systems work much the same way as direct expansion systems work. The exception is they use water in the coil rather than refrigerant. Technically speaking, water can be classified as a refrigerant. Chilled Water systems can be rather complex and many chilled water systems are found in commercial and industrial applications. There are some chilled water systems used in residential applications.</p>\r\n\r\n<p>However, chilled water systems in residential HVAC systems are extremely rare. A typical chiller uses the process of refrigeration to chill water in a chiller barrel. This water is pumped through chilled water piping throughout the building where it will pass through a coil. Air is passed over this coil and the heat exchange process takes place. The heat in the air is absorbed into the coils and then into the water. The water is pumped back to the chiller to have the heat removed. It then makes the trip back through the building and the coils all over again.</p>\r\n\r\n<p>⥤ &nbsp;&nbsp;<strong>Ducting</strong></p>\r\n\r\n<p>HVAC (an initialism meaning &ldquo;heating, ventilation and air conditioning&rdquo;) systems are something that greatly benefits our modern society, but is often taken for granted. HVAC systems are necessary to keep indoor spaces warm, cool, and full of high-quality and clean air. In order to have an effective system, you need to have quality HVAC air ducts. At Ducting.com, we specialize in flexible hoses for heating, cooling, and venting in outside air to maintain high-quality air control. Whether you want to cool a car, house, or industrial-sized building, we have the right air duct hoses for the task.</p>\r\n\r\n<p>⥤ &nbsp;&nbsp;<strong>Installation &amp; Maintenance</strong></p>\r\n\r\n<p>A new HVAC system represents a big investment, and it&rsquo;s understandable that some home or business owners will want to cut costs on installation. This can be a grave error if they pick an unqualified contractor to install air conditioning units, as a shoddy installation job can result in long-term problems and expenses.</p>\r\n\r\n<p>Heating and air conditioning units and their associated ductwork and infrastructure are complex systems that require precise installation. If the job&rsquo;s not done right, the system won&rsquo;t perform efficiently and may be more prone to breakage and have a shorter life. Energy Star, a federal program that evaluates appliances for efficiency and environmental impact, estimates that about half of all heating and cooling systems don&rsquo;t perform as they should because they were improperly installed.</p>\r\n\r\n<p>An HVAC system that&rsquo;s not providing optimum performance can be costly for home and business owners. HVAC systems account for about 40 percent or more of our energy consumption. Energy Star estimates that improper installation can reduce system efficiency by up to 30 percent.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647325166/llqios9vqjiez8ejfy19.png','hvac-projects-division'),(5,'2022-03-15 00:50:12','2022-03-15 00:50:12',NULL,'Safety Supports','Occupational Health & Safety Services and Project Support','<p>We are providing Occupational Health Safety and Life safety supports to the projects with NEBOSH and other certified Engineers / Officers.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647325211/qqtphvbxfx6auvkzflrm.png','safety-supports'),(6,'2022-03-15 00:50:50','2022-03-15 00:50:50',NULL,'Shutdown projects Supports','Shut Down projects Division','<p><strong>Shut Down projects Division:&nbsp;</strong>ATS provides Electrical, Mechanical, Technical and manpower support to plant and factory shutdown projects, Oil &amp; Gas, and Marine industry. ATS Shutdown support team have experience in repairs and maintenance of Engine, turbine, heat exchangers, pumps, valves, motors, generators, compressors, etc.</p>\r\n\r\n<p>Our shutdown Projects manpower support is committed with qualified Engineers, Foremen, Draftsmen, Safety Officers, Supervisors, Marine Technicians, Valve Technicians, Pump Technicians Heat Exchangers Technicians, Turbocharger Specialist, Riggers, Plumbers, Duct Men, Steel Fixtures, Electricians, Pipe Fitters, Qualified Welders, Generator Technicians, Masons, Carpenters, Interior and Exterior Painters, Light and Heavy Duty Drivers, Cleaners etc.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647325250/ntj7gbss3xyte0lxybut.png','shutdown-projects-supports'),(7,'2022-03-15 00:51:30','2022-03-15 00:51:30',NULL,'Trading','Trading Division','<p>ATS imports and supplies all types of Electrical and Electronics Devices and equipment, Testing Tools, Fire Fighting, Fire Detection, Fire Suppression materials, Fire protection tools and equipment&rsquo;s, Pipe and Fittings, All kinds of personal protective equipment&rsquo;s (PPEs), Electromechanical Tools &amp; Equipment&rsquo;s, and Supply of Building construction tools and materials, etc.</p>','https://res.cloudinary.com/dxxlsebas/image/upload/v1647325290/y9mcmssqcwywtnpbyppm.png','trading');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonials` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial` text COLLATE utf8mb4_unicode_ci,
  `who_is_who` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'atsdoha','atsdoha@gmail.com',NULL,'$2y$10$mYO1i5xE4xY46EOo45DrheHW2yq5joxNB8ZBiEfWba21atjm9K1WK',NULL,'2022-03-14 06:54:14','2022-03-14 06:54:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-20 13:02:24
